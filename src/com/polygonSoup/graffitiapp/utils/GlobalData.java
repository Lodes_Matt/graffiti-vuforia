package com.polygonSoup.graffitiapp.utils;

import android.app.Application;
import android.graphics.Bitmap;

public class GlobalData extends Application {
	
	private Bitmap mImage;
	private String mOverlayPath;
	private String mImgPath;
	private Bitmap mOverlay;
	private boolean mImgIsCurrent = false;
	public Bitmap getmOverlay() {
		return mOverlay;
	}
	public void setmOverlay(Bitmap mOverlay) {
		this.mOverlay = mOverlay;
	}
	
	public Bitmap getmImage() {
		return mImage;
	}
	public void setmImage(Bitmap mImage) {
		this.mImage = mImage;
	}
	public String getmName() {
		return mOverlayPath;
	}
	public void setmName(String mName) {
		this.mOverlayPath = mName;
	}
	public boolean ismImgIsCurrent() {
		return mImgIsCurrent;
	}
	public void setmImgIsCurrent(boolean mImgIsCurrent) {
		this.mImgIsCurrent = mImgIsCurrent;
	}
	public String getmOverlayPath() {
		return mOverlayPath;
	}
	public void setmOverlayPath(String mOverlayPath) {
		this.mOverlayPath = mOverlayPath;
	}
	public String getmImgPath() {
		return mImgPath;
	}
	public void setmImgPath(String mImgPath) {
		this.mImgPath = mImgPath;
	}

}
