package com.polygonSoup.graffitiapp.utils;

public class FilenameGenerator {

	public static String generateNewFilename() {
		return java.util.UUID.randomUUID().toString()+".png";
	}

}
