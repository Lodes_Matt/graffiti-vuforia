package com.polygonSoup.graffitiapp.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.util.Log;

public class ImageUtils {

	public static Bitmap rotate(String path) {
		Bitmap bm = BitmapFactory.decodeFile(path);
        ExifInterface exif = null;
		try {
			exif = new ExifInterface(path);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90){
        	rotationAngle = 90;
        }
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180){
        	rotationAngle = 180;
        }
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270){
        	rotationAngle = 270;
        }

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
        //lazily write it back
        new ImageUtils.ImgWriteTask(rotatedBitmap, path).execute();
        return rotatedBitmap;
		
	}
	
	public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
	    return ThumbnailUtils.extractThumbnail(bm, newWidth, newHeight);
	}
	
	

	public static void writeImage(Bitmap finalBitmap, String path) {
		String compress = path.substring(path.lastIndexOf('.'));
		Log.d("ImageWriter", compress);
		File f = new File(path);
		if (f.exists()) {
			f.delete();
		}
		try {
			FileOutputStream out = new FileOutputStream(f);
			if (compress.equalsIgnoreCase(".jpg")) {
				finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			} else if (compress.equalsIgnoreCase(".png")) {
				finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			} else {
				Log.e("ImageWriter", "couldnt determine format of source image");
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static class ImgWriteTask extends AsyncTask<Void, Void, Void>{
		private Bitmap bmp;
		private String path;

		public ImgWriteTask(Bitmap rotated, String path) {
			bmp = rotated;
			this.path = path;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... params) {
			ImageUtils.writeImage(bmp, path);
			return null;
		}
		
		
	}

}