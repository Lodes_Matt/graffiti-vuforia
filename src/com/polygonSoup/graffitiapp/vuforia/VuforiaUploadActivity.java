package com.polygonSoup.graffitiapp.vuforia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.polygonSoup.graffitiapp.ui.AbstractNavDrawerActivity;
import com.polygonSoup.graffitiapp.utils.GlobalData;

public class VuforiaUploadActivity extends AbstractNavDrawerActivity {

	private String mCheckServerUrl = "http://haxxortheplanet.koerce.me/graffiti/conncheck.php";
	private String mPostServerUrl = "http://haxxortheplanet.koerce.me/graffiti/select.php?select=postnewtarget";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		 * check for internet. we should do this anytime we do network stuff. if
		 * we dont have network, notify and stop trying. maybe make this an
		 * alertdialog?
		 */
		new ConnectionCheck().execute();

	}

	// callback for conncheck
	protected void doVuforiaUploadTask() {
		// make the progress dialog
		ProgressDialog pd = ProgressDialog.show(this, "",
				"Uploading to vuforia...", true);
		// do the upload
		new ImageUploader(pd, this).execute();
	}

	private class ConnectionCheck extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			HttpClient http = new DefaultHttpClient();
			HttpGet get = new HttpGet(mCheckServerUrl);
			ResponseHandler<String> rh = new BasicResponseHandler();
			String response;
			try {
				response = http.execute(get, rh);
				if (response.equals("hello")) {
					return true;
				} else {
					return false;
				}

			} catch (ClientProtocolException e) {
				return false;
			} catch (IOException e) {
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (!result) {
				Toast.makeText(
						VuforiaUploadActivity.this,
						"Can't get an internet connection. Please make sure you are connected to the Internet",
						Toast.LENGTH_LONG).show();
				finish(); // quit, we have no network
			} else {
				doVuforiaUploadTask();
			}
		}
	}

	/**
	 * This is the AsyncTask that does the uploading. Android does not allow net
	 * on the main thread
	 * 
	 */
	private class ImageUploader extends AsyncTask<Void, Void, String> {

		private ProgressDialog pd;
		private Activity a;

		public ImageUploader(ProgressDialog pd, Activity a) {
			this.pd = pd;
			this.a = a;
		}

		// if this is about to run, show the progress dialog
		@Override
		protected void onPreExecute() {
			pd.show();
		}

		// do the upload
		@Override
		protected String doInBackground(Void... params) {
			// grab the image by path, since vuforia expects
			// a PNG
			GlobalData gd = (GlobalData) getApplication();
			File imageTarget = new File(gd.getmImgPath());
			Log.d("vua", gd.getmImgPath());
			// read the whole file into a byte array
			if (!imageTarget.exists() || !imageTarget.canRead()) {
				return "Couldn't find picture for upload";
			}
			// vuforia cant handle a file bigger than 2,25MB
			if (imageTarget.length() > 2.25 * 1024 * 1024) {
				return "Image too big";
			}
			FileInputStream image_reader;
			byte[] image = new byte[(int) imageTarget.length()];
			try {
				image_reader = new FileInputStream(imageTarget);
				image_reader.read(image);
				image_reader.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				return "Image upload was unsuccessful";
			} catch (IOException e) {
				e.printStackTrace();
				return "Image upload was unsuccessful";
			}
			// we have the image. now we do the same thing with the overlay
			File overlayFile = new File(gd.getmOverlayPath());
			if (!overlayFile.exists() || !overlayFile.canRead()) {
				return "Couldnt find drawing for upload";
			}
			// overlay has no size limit
			FileInputStream overlay_reader;
			byte[] overlay = new byte[(int) overlayFile.length()];
			try {
				overlay_reader = new FileInputStream(overlayFile);
				overlay_reader.read(overlay);
				overlay_reader.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				return "Image upload was unsuccessful";
			} catch (IOException e) {
				e.printStackTrace();
				return "Image upload was unsuccessful";
			}
			// get the part of the overlay name the database cares about
			// note that this also works for vuforia targetName as they have
			// the same uuid
			String oname = overlayFile.getName().substring(0,
					overlayFile.getName().lastIndexOf('.') - 1);
			// now we have everything and can upload
			// vuforia wants the file in base64. default flags are ok
			// as this will be in the request body
			String image_b64 = Base64.encodeToString(image, Base64.DEFAULT);
			String ov_b64 = Base64.encodeToString(oname.getBytes(),
					Base64.DEFAULT);
			String draw_b64 = Base64.encodeToString(overlay, Base64.DEFAULT);
			// vuforia/our server expects nothing but JSON in the request
			// body
			JSONObject req = new JSONObject();
			try {
				req.put("name", oname); // target name
				req.put("image", image_b64); // image data
				req.put("active_flag", true); // this is an active target
				req.put("width", 320.0); // i dont know what this does, but
											// i saw demos with 320 and its
											// required
				req.put("overlay", draw_b64); // overlay. not actually sent
												// to vuforia, just our
												// server
				req.put("overlay_name", ov_b64); // overlay name. this is
													// sent to vuforia
			} catch (JSONException e) {
				e.printStackTrace();
				return "Image upload was unsuccessful";
			}
			// post to the server. we dont need any special headers, server
			// takes care of that
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(mPostServerUrl);
			try {
				post.setEntity(new StringEntity(req.toString()));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return "Image upload was unsuccessful";
			}
			try {
				ResponseHandler<String> rh = new BasicResponseHandler();
				String response = client.execute(post, rh);
				// parse the result code out of the string. the webserver
				// sends back extra
				// info rather than just the JSON
				int start = response.indexOf("\"result_code\":\"") + 15;
				int end = response.indexOf(',', start) - 1;
				String result = response.substring(start, end);
				if (result.equalsIgnoreCase("targetcreated")) {
					gd.setmImgIsCurrent(false);
					return "Image uploaded successfully";
				} else if (result.equalsIgnoreCase("targetexists")) {
					return "This image has already been uploaded. Augment that image";
				} else {
					Log.e("vuforiaupload", result);
					return "Image upload was unsuccessful";
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				return "Image upload was unsuccessful";
			} catch (IOException e) {
				e.printStackTrace();
				return "Image upload was unsuccessful";
			}

		}

		// show toast, hide progress dialog, kill activity
		@Override
		protected void onPostExecute(final String rc) {
			Toast.makeText(getApplicationContext(), rc, Toast.LENGTH_SHORT)
					.show();

			if (pd.isShowing()) {
				pd.dismiss();
			}
			a.finish();
		}
	}
}