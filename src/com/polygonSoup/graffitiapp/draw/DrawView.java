package com.polygonSoup.graffitiapp.draw;

import java.util.ArrayList;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View {
	public Paint paint = new Paint();
	private Path path;
	private int color = Color.CYAN;
	private float strokeWidth;
	private LayoutParams params;
	private ArrayList<Path> pathList = new ArrayList<Path>();
	private ArrayList<Path> removedPathList = new ArrayList<Path>();
	
	private ArrayList<Paint> paintList = new ArrayList<Paint>();
	private ArrayList<Paint> removedPaintList = new ArrayList<Paint>();
	
	private boolean erasing = false;
	
	private float x, y;
    private static final float TOUCH_TOLERANCE = 4f;
	
	
	private Canvas canvas;
	
	private Bitmap bmp;
	
	
	public DrawView(Context context) {
		super(context);
		initVars();
		initPaint();
		params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		setDrawingCacheEnabled(true);

	}
	
	/**
	 * Initializes private variables to default values
	 */
	private void initVars(){
		setStrokeWidth(10f);
		this.params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		path = new Path();
		bmp = Bitmap.createBitmap (1, 1, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bmp);
		canvas.drawColor(0, Mode.CLEAR);
		
	}
	
	/**
	 * Initializes the Paint Object 
	 */
	private void initPaint(){
		paint.setAntiAlias(true);
		paint.setColor(color);
		paint.setDither(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeWidth(this.strokeWidth);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh){
		super.onSizeChanged(w, h, oldw, oldh);
		bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bmp);
		canvas.drawColor(0, Mode.CLEAR);
	}
	
	/**
	 * Return the params
	 * @return params
	 */
	public LayoutParams getParams(){
		return this.params;
	}
	
	/**
	 * Gets the color that is to be drawn to the screen
	 * @return Color color - the color that is being drawn
	 */
	public int getColor() {
		return color;
	}

	/**
	 * Sets the color to be drawn to the screen
	 * @param color - single integer that represents the color
	 */
	public void setColor(int color) {
		this.color = color;
	}
	
	/**
	 * Sets the color to be drawn to the screen
	 * @param r - int red component of the color
	 * @param g - int green component of the color
	 * @param b - int blue component of the color
	 * @param color 
	 */
	public void setColor(int r, int g, int b){
		color = Color.rgb(r,g,b);
		this.paint.setColor(color);
	}

	/**
	 * Gets the width of the paint stroke
	 * @return float - width of the stroke
	 */
	public float getStrokeWidth() {
		return strokeWidth;
	}
	
	/**
	 * Sets the width of the paint stroke
	 * @param strokeWidth - Positive float that represents width of the stroke
	 */
	public void setStrokeWidth(float strokeWidth) {
		this.strokeWidth = Math.abs(strokeWidth);
	}
	
	/**
	 * While the screen is touched, adds a series of connected paths
	 * when the user moves 
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event){
		float xCoordinate = event.getX();
		float yCoordinate = event.getY();

		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			path.moveTo(xCoordinate, yCoordinate);
			x = xCoordinate;
			y = yCoordinate;
			return true;
			
		case MotionEvent.ACTION_MOVE:
			float deltaX = Math.abs(xCoordinate - x);
			float deltaY = Math.abs(yCoordinate - y);
			if(deltaX >= TOUCH_TOLERANCE || deltaY >= TOUCH_TOLERANCE){
				path.quadTo(x, y, (xCoordinate + x)/2, (yCoordinate + y)/2);
				x = xCoordinate;
				y= yCoordinate;
			}
			break;
			
		case MotionEvent.ACTION_UP:
			this.pathList.add(this.path);
			this.paintList.add(this.paint);
			path.lineTo(x, y);
			paint.setColor(color);
			canvas.drawPath(path, paint);
			path = new Path();
			if(erasing){
				erase_mode();
			}
			paint = new Paint();
			initPaint();
			break;
			
		default:
			return false;
		}
		// Force re-draw the screen
		invalidate();
		return false;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		this.canvas = canvas;
		canvas.drawColor(Color.TRANSPARENT);
		paint.setColor(color);
		canvas.drawPath(path, paint);
		for(int index = 0; this.pathList.size() > index; index ++){
			canvas.drawPath(this.pathList.get(index), this.paintList.get(index));
		}
		
	}
	
	/**
	 * Removes the previous path drawn
	 */
	public void undo(){
		int pathSize = pathList.size();
		int paintSize = paintList.size();

		if(pathSize > 0){
			this.removedPathList.add(pathList.get(pathSize - 1));
			pathList.remove(pathSize - 1); 
		}
		if(paintSize > 0){
			this.removedPaintList.add(paintList.get(pathSize - 1));
			paintList.remove(pathSize - 1); 
		}
		invalidate();
	}
	
	public void redo(){
		if(!this.removedPathList.isEmpty()){
			pathList.add(removedPathList.get(removedPathList.size() - 1));
			removedPathList.remove(removedPathList.size() - 1);
		}
		if(!this.removedPaintList.isEmpty()){
			paintList.add(removedPaintList.get(removedPaintList.size() - 1));
			removedPaintList.remove(removedPaintList.size() - 1);
		}
		invalidate();
	}
	
	/**
	 * Deletes all of the paths drawn
	 */
	public void deleteAll(){
		while(!pathList.isEmpty()){
			undo();
		}
		invalidate();

	}
	
	public Bitmap getBitmap(){
	    return Bitmap.createBitmap(getDrawingCache());
	}
	
	public void erase_mode() {
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		erasing = true;
	}




	public void draw_mode() {
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
		erasing = false;
	}
	
	
}
