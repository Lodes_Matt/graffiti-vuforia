package com.polygonSoup.graffitiapp.draw;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.polygonSoup.graffitiapp.R;
import com.polygonSoup.graffitiapp.ui.AbstractNavDrawerActivity;
import com.polygonSoup.graffitiapp.utils.GlobalData;
import com.polygonSoup.graffitiapp.utils.ImageUtils;
import com.polygonSoup.graffitiapp.vuforia.VuforiaUploadActivity;

public class DrawActivity extends AbstractNavDrawerActivity {
	
	private DrawView mDrawView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mDrawerList.setItemChecked(2, true);
		mDrawView = new DrawView(this);
		
		FrameLayout f = (FrameLayout)findViewById(R.id.content_frame);
		f.addView(mDrawView);
	}
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	
	
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent();
		i.putExtra("finish", true);
		setResult(RESULT_OK, i);
		finish();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.draw_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){
		case R.id.save:
			Bitmap b = mDrawView.getBitmap();
			GlobalData gd = (GlobalData) getApplication();
			gd.setmOverlay(b);
			ImageUtils.writeImage(b, gd.getmOverlayPath());
			showUploadDialog(this);
			break;
		case R.id.changeColor:
			AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, mDrawView.getColor(),
					new OnAmbilWarnaListener(){
				@Override
				public void onOk(AmbilWarnaDialog dialog, int color){
					mDrawView.setColor(color);
				}

				@Override
				public void onCancel(AmbilWarnaDialog dialog) {
					//dont do anything on cancel
				}
			});
			dialog.show();
			break;
		case R.id.reset:
			mDrawView.deleteAll();
			break;
		case R.id.undo:
			mDrawView.undo();
			break;
		case R.id.redo:
			mDrawView.redo();
			break;
		case R.id.erase_mode:
			mDrawView.erase_mode();
			break;
		case R.id.draw_mode:
			mDrawView.draw_mode();
			break;
		default:
			break;
		}
		return true;
	}
	
	private void showUploadDialog(final Context ctx){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
				
				
				builder.setMessage(R.string.dialog_upload_message).setTitle(R.string.dialog_upload_title)
					.setPositiveButton(R.string.dialog_upload_ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id){
							Intent i = new Intent(ctx, VuforiaUploadActivity.class);
							dialog.dismiss();
							startActivityForResult(i, 2377);
							
							
						}
					})
					.setNegativeButton(R.string.dialog_upload_cancel, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id){
							dialog.dismiss();
						}
					}).show();
				
		}
	
	public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent){
		
	}
	

}
