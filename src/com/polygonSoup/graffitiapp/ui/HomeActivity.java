package com.polygonSoup.graffitiapp.ui;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.haarman.listviewanimations.swinginadapters.prepared.AlphaInAnimationAdapter;
import com.polygonSoup.graffitiapp.R;
import com.polygonSoup.graffitiapp.utils.ImageUtils;

/**
 * @author kalmiya
 * 
 */
public class HomeActivity extends AbstractNavDrawerActivity {

	private static final String mServerURL = "http://haxxortheplanet.koerce.me/graffiti/homeimages.php";
	private ArrayList<Bitmap> mBitmaps = new ArrayList<Bitmap>(4);
	public static ArrayList<String> mUrls = new ArrayList<String>(4);
	private FrameLayout f;
	private int mScreenWidth;
	private int mScreenHeight;
	private int mNumImagesOnScreen = 3;
	private ImageAdapter mAdapter;
	private LayoutInflater mInflater;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(mPageTitles[0]);
		mDrawerList.setItemChecked(0, true);

		// add our layout
		f = (FrameLayout) findViewById(R.id.content_frame);
		
		DisplayMetrics metrics = new DisplayMetrics();

		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		mScreenWidth = metrics.widthPixels;
		mScreenHeight = metrics.heightPixels;
		final TypedArray styledAttributes = getBaseContext().getTheme()
				.obtainStyledAttributes(
						new int[] { android.R.attr.actionBarSize });
		int ab_sz = (int) styledAttributes.getDimension(0, 0);
		styledAttributes.recycle();
		mScreenHeight -= ab_sz;
		
		new GetImagesTask().execute();
		mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout homeLayout = (LinearLayout) mInflater.inflate(
				R.layout.activity_home, null);
		ListView image_list = (ListView) homeLayout.findViewById(R.id.home_images_list);
		f.addView(homeLayout);
		mAdapter = new ImageAdapter(R.layout.image_list_view);
		AlphaInAnimationAdapter animAdapter = new AlphaInAnimationAdapter(
				mAdapter);
		animAdapter.setAbsListView(image_list);
		image_list.setAdapter(animAdapter);
		image_list.setOnItemClickListener(this);

	}

	private class ImageAdapter extends BaseAdapter {

		private ArrayList<Bitmap> data;
		private int viewId;

		public ImageAdapter(int imageListViewid) {
			viewId = imageListViewid;
			data = mBitmaps;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowview = mInflater.inflate(viewId, null);
			ImageView iv = (ImageView) rowview.findViewById(R.id.image_home);
			Bitmap b = data.get(position);
			if (b != null) {
				iv.setImageBitmap(b);
			} else {
				Log.e("Graffiti.homeactivity",
						"couldnt find previously downloaded image");
			}
			return rowview;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

	}

	private class GetImagesTask extends AsyncTask<Void, Void, Void> {
		private TransparentProgressDialog progressDialog;

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
			mAdapter.notifyDataSetChanged();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			mAdapter.notifyDataSetChanged();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = TransparentProgressDialog.show(HomeActivity.this,
					null, null, true, false, null);

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// we need to hit the server
			try {
				HttpClient http = new DefaultHttpClient();
				HttpGet get = new HttpGet(mServerURL);
				ResponseHandler<String> rh = new BasicResponseHandler();
				String response = http.execute(get, rh);
				// our server returns json
				JSONObject jsonObject = new JSONObject(response);
				JSONArray urlArr = jsonObject.getJSONArray("urls");
				Log.d("json", "length:" + urlArr.length());
				ByteArrayBuffer bab = new ByteArrayBuffer(2359296); // 2.25MB
				for (int i = 0; i < urlArr.length(); ++i) {
					String urlstr = urlArr.getString(i);
					// we cant use httpclient for this as we are getting raw
					// bytes
					
					URL url = new URL(urlstr);
					URLConnection conn = url.openConnection();
					InputStream is = conn.getInputStream();
					BufferedInputStream bis = new BufferedInputStream(is);
					int currbyte = 0;
					while ((currbyte = bis.read()) != -1) {
						bab.append(((byte) currbyte));
					}
					Bitmap b = BitmapFactory.decodeByteArray(bab.toByteArray(),
							0, bab.length());
					// if it needs resizing, do it. we want to fit num
					// images on the screen at once
					// so each image needs to be at maximum 1/3 of
					// screen width and height
					if (b != null) {
						Bitmap newb = ImageUtils.getResizedBitmap(b, mScreenWidth,
								mScreenHeight / mNumImagesOnScreen);
						mBitmaps.add(newb);
						mUrls.add(urlstr);
						b.recycle();
					} else {
						Log.e("Graffiti.HomeActivity", "got a corrupt image");
					}
					bab.clear();
				}
			} catch (MalformedURLException e) {
				Log.e("graffiti.homeactivity.getimages", "url was bad");
				e.printStackTrace();
			} catch (IOException e) {
				Log.e("graffiti.homeactivity.getimages", "unknown error");
				e.printStackTrace();
			} catch (JSONException e) {
				Log.e("graffiti.homeactivity.getimages",
						"response from the server was wonky");
				e.printStackTrace();
			}
			return null;
		}

	}

}
