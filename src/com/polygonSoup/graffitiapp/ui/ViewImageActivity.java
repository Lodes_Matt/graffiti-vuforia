package com.polygonSoup.graffitiapp.ui;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

import com.polygonSoup.graffitiapp.R;
import com.polygonSoup.graffitiapp.draw.DrawActivity;
import com.polygonSoup.graffitiapp.utils.GlobalData;

public class ViewImageActivity extends AbstractNavDrawerActivity {
	
	private String mUrl;
	private ImageView iv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(mPageTitles[2]);
		mDrawerList.setItemChecked(2, true);
		Intent i = getIntent();
		if(i.hasExtra("url")){
			String s = i.getStringExtra("url");
			
			if(s != null){
				try {
					new DownloadImageTask(new URL(s), this).execute();
					
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else{
			GlobalData gd = (GlobalData) getApplication();
			// set the view of the content frame
			FrameLayout f = (FrameLayout) findViewById(R.id.content_frame);
			iv = new ImageView(this);
			
			iv.setImageBitmap(gd.getmImage());

			iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));
			f.addView(iv);

			Intent intent = new Intent(this, DrawActivity.class);
			startActivityForResult(intent, 666);
		}
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent i) {
		super.onActivityResult(requestCode, resultCode, i);
		if (requestCode == 666) {
			finish();
		}
	}
	
	private class DownloadImageTask extends AsyncTask<Void,Void,Void>{
		private TransparentProgressDialog progressDialog;
		private URL url;
		private FrameLayout f;
		private ImageView iv;
		private Context c;
		private Bitmap mB = null;
		
		public DownloadImageTask(URL url, Context c) {
			super();
			this.url = url;
			this.c = c;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(mB != null){
				f = (FrameLayout) findViewById(R.id.content_frame);
				iv = new ImageView(c);
				iv.setImageBitmap(mB);
				iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.MATCH_PARENT));
				f.addView(iv);
			}
			progressDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = TransparentProgressDialog.show(ViewImageActivity.this,
					null, null, true, false, null);

		}


		@Override
		protected Void doInBackground(Void... params) {
			ByteArrayBuffer bab = new ByteArrayBuffer(2359296);
			URLConnection conn;
			try {
				conn = url.openConnection();
				InputStream is = conn.getInputStream();
				BufferedInputStream bis = new BufferedInputStream(is);
				int currbyte = 0;
				while ((currbyte = bis.read()) != -1) {
					bab.append(((byte) currbyte));
				}
				Bitmap b = BitmapFactory.decodeByteArray(bab.toByteArray(),
						0, bab.length());
				if(b != null){
					mB  = b;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
	}
}
