package com.polygonSoup.graffitiapp.ui;

/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.FileNotFoundException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.polygonSoup.graffitiapp.utils.FilenameGenerator;
import com.polygonSoup.graffitiapp.utils.GlobalData;
import com.polygonSoup.graffitiapp.vuforia.SearchActivity;
import com.polygonSoup.graffitiapp.R;

public abstract class AbstractNavDrawerActivity extends Activity implements
		ListView.OnItemClickListener {

	private static final int SELECT_PHOTO_REQUEST_CODE = 1337;
	private DrawerLayout mDrawerLayout;
	protected ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	protected CharSequence mDrawerTitle;
	protected CharSequence mTitle;
	protected String[] mPageTitles;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminateVisibility(true);
		setContentView(R.layout.activity_abstract_nav);

		mTitle = mDrawerTitle = getTitle();
		mPageTitles = getResources().getStringArray(R.array.page_titles_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.nav_drawer);

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mPageTitles));
		mDrawerList.setOnItemClickListener(this);

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	protected void selectItem(int index, Context ctx) {
		Intent nxt;
		switch (index) {
		case 0:
		default:
			nxt = new Intent(ctx, HomeActivity.class);
			startActivity(nxt);
			break;
		case 1:
			nxt = new Intent(ctx, CaptureImageActivity.class);
			startActivity(nxt);
			break;
		case 2:
			GlobalData gd = (GlobalData) getApplication();
			if(gd.ismImgIsCurrent()){
				nxt = new Intent(ctx, ViewImageActivity.class);
				startActivity(nxt);
			} else{
				nxt = new Intent(Intent.ACTION_PICK);
				nxt.setType("image/*");
				startActivityForResult(nxt, SELECT_PHOTO_REQUEST_CODE);
			}
			break;
		case 3:
			nxt = new Intent(ctx, SearchActivity.class);
			startActivity(nxt);
			break;
		case 4:
			nxt = new Intent(ctx, OpenSourceLicenseDisplayActivity.class);
			startActivity(nxt);
			break;
		}
		if (this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
			mDrawerList.setItemChecked(index, true);
            mDrawerLayout.closeDrawer(mDrawerList);
            mDrawerList.setItemChecked(index, false);
        }

	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            if ( this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
                this.mDrawerLayout.closeDrawer(this.mDrawerList);
            }
            else {
                this.mDrawerLayout.openDrawer(this.mDrawerList);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
	    super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
	    switch(requestCode) { 
	    case SELECT_PHOTO_REQUEST_CODE:
	        if(resultCode == RESULT_OK){  
	            Uri selectedImage = imageReturnedIntent.getData();
	            InputStream imageStream = null;
				try {
					imageStream = getContentResolver().openInputStream(selectedImage);
				} catch (FileNotFoundException e) {
					Log.e("ImagePicker", "Image was wonky, bailing");
					e.printStackTrace();
					selectItem(0, this);
				}
	            Bitmap b = BitmapFactory.decodeStream(imageStream);
	            GlobalData gd = (GlobalData) getApplication();
	            gd.setmImage(b);
	            gd.setmImgPath(getRealPathFromURI(selectedImage));
	            gd.setmOverlayPath(getExternalFilesDir(null)+"/saved_overlays/"+FilenameGenerator.generateNewFilename());
	            gd.setmImgIsCurrent(true);
	            selectItem(2, this);
	        }
	    }
	}
	
	private String getRealPathFromURI(Uri contentUri) {
	    String[] proj = { MediaStore.Images.Media.DATA };
	    CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
	    Cursor cursor = loader.loadInBackground();
	    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    cursor.moveToFirst();
	    return cursor.getString(column_index);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View arg1, int idx, long arg3) {
		Log.d("test", String.valueOf(adapter.getId()));
		switch (adapter.getId()){
			case R.id.nav_drawer:
				selectItem(idx, this);
				break;
			case R.id.home_images_list:
				Intent i = new Intent(this, ViewDownloadedImageActivity.class);
				i.putExtra("url", HomeActivity.mUrls.get(idx));
				startActivity(i);
				
		}

	}

	/*
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

}
