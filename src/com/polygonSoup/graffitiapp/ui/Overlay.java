package com.polygonSoup.graffitiapp.ui;


import android.graphics.Bitmap;

/**
 * Class that holds all of the overlay information and stuff
 * but it is really just the image
 * @author mattlodes
 */
public class Overlay
{
    private Bitmap overlay;
    private String overlayURL;


    public Overlay()
    {

    }
    
    
	public Bitmap getOverlayImage() {
		return overlay;
	}
	
	public void setOverlayImage(Bitmap overlay) {
		this.overlay = overlay;
	}
	
	public String getOverlayURL() {
		return overlayURL;
	}
	
	public void setOverlayURL(String overlayURL) {
		this.overlayURL = overlayURL;
	}
	
    public void recycle()
    {
        // cleans up the overlay image
        overlay.recycle();
        overlay = null;
    }
}
