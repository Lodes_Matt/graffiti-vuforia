package com.polygonSoup.graffitiapp.ui;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.polygonSoup.graffitiapp.R;

public class ViewDownloadedImageActivity extends AbstractNavDrawerActivity {

	private Bitmap mBitmap = null;

	private ImageViewHandler mHandler;
	private ImageView iv = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("View Image");
		mDrawerList.setItemChecked(0, true);
		Intent i = getIntent();
		if (i.hasExtra("url")) {
			try {
				mHandler = new ImageViewHandler(this);
				FrameLayout f = (FrameLayout) findViewById(R.id.content_frame);
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				LinearLayout ll = (LinearLayout) inflater.inflate(
						R.layout.activity_view_downloaded_image, null);
				iv = (ImageView) ll.findViewById(R.id.downloaded_image_view);
				f.addView(ll);
				URL url = new URL(i.getStringExtra("url"));
				new DownloadImageFromURLTask(url).execute();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class DownloadImageFromURLTask extends AsyncTask<Void, Void, Void> {
		private URL mUrl;
		private TransparentProgressDialog progressDialog;

		public DownloadImageFromURLTask(URL url) {
			mUrl = url;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			mHandler.sendEmptyMessage(2323);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = TransparentProgressDialog.show(
					ViewDownloadedImageActivity.this, null, null, true, false,
					null);
		}

		@Override
		protected Void doInBackground(Void... params) {
			URLConnection conn;
			try {
				conn = mUrl.openConnection();
				InputStream is = conn.getInputStream();
				BufferedInputStream bis = new BufferedInputStream(is);
				ByteArrayBuffer bab = new ByteArrayBuffer(2359296); // 2.25MB
				int currbyte = 0;
				while ((currbyte = bis.read()) != -1) {
					bab.append(((byte) currbyte));
				}
				Bitmap b = BitmapFactory.decodeByteArray(bab.toByteArray(), 0,
						bab.length());
				if (b != null) {
					mBitmap = b;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

	}

	private static class ImageViewHandler extends Handler {
		
		private WeakReference<ViewDownloadedImageActivity> mActivity;
		
		public ImageViewHandler(ViewDownloadedImageActivity vdia){
			mActivity = new WeakReference<ViewDownloadedImageActivity>(vdia);
		}
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 2323 && mActivity != null) {
				ViewDownloadedImageActivity vdia = mActivity.get();
				if (vdia.iv != null && vdia.mBitmap != null) {
					vdia.iv.setImageBitmap(vdia.mBitmap);
				}
			}
		}
	}
}
