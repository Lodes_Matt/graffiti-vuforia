package com.polygonSoup.graffitiapp.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.polygonSoup.graffitiapp.R;

public class OpenSourceLicenseDisplayActivity extends AbstractNavDrawerActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(mPageTitles[4]);
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.activity_license_display, null);
		TextView tv = (TextView) v.findViewById(R.id.license_tv);
		FrameLayout f = (FrameLayout) findViewById(R.id.content_frame);
		f.addView(tv);
		tv.setMovementMethod(new ScrollingMovementMethod());
		
	}

	
}
