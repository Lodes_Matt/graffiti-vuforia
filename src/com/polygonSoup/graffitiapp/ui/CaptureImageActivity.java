package com.polygonSoup.graffitiapp.ui;

import java.io.File;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.polygonSoup.graffitiapp.utils.FilenameGenerator;
import com.polygonSoup.graffitiapp.utils.GlobalData;
import com.polygonSoup.graffitiapp.utils.ImageUtils;

public class CaptureImageActivity extends AbstractNavDrawerActivity {
	private static final int CAPTURE_IMAGE_REQUEST_CODE = 777;
	private String mPath;
	private String mOPath;
	private Uri uri;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(mPageTitles[1]);
		mDrawerList.setItemChecked(1, true);
		
	    File myDir = new File(getExternalFilesDir(null) + "/saved_camera_images/");    
	    if(!(myDir.mkdirs() || myDir.isDirectory())) {
	    	Toast.makeText(this, "Couldnt access your external storage. Please check your storage device", 
	    			Toast.LENGTH_LONG).show();
	    	finish();
	    }
	    File myODir = new File(getExternalFilesDir(null) + "/saved_overlays/");    
	    if(!(myODir.mkdirs() || myODir.isDirectory())) {
	    	Toast.makeText(this, "Couldnt access your external storage. Please check your storage device", 
	    			Toast.LENGTH_LONG).show();
	    	finish();
	    }
	    String fname = FilenameGenerator.generateNewFilename();
	    mOPath = getExternalFilesDir(null)+"/saved_overlays/"+fname;
	    File file = new File (myDir, fname);
	    uri = Uri.fromFile(file);
	    mPath = uri.getPath();
	    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    i.putExtra(MediaStore.EXTRA_OUTPUT, uri);
	    startActivityForResult(i, CAPTURE_IMAGE_REQUEST_CODE);
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == CAPTURE_IMAGE_REQUEST_CODE) {
	        if (resultCode == RESULT_OK) {
	            Toast.makeText(this, "Image captured.", Toast.LENGTH_LONG).show();
	            Bitmap b = ImageUtils.rotate(mPath);
	            if(b != null){
	            	GlobalData gd = (GlobalData) getApplication();
	            	gd.setmImage(b);
	            	gd.setmImgPath(mPath);
	            	gd.setmOverlayPath(mOPath);
	            	gd.setmImgIsCurrent(true);
	            	selectItem(2, this);
	            	finish();
	            } else{
	            	Log.e("captureImage", "Image was null! baliing");
	            	finish();
	            }
	        } else if (resultCode == RESULT_CANCELED) {
	        	Toast.makeText(this, "Image capture cancelled", Toast.LENGTH_LONG).show();
	        	finish();
	        } else {
	        	Toast.makeText(this, "Camera service error", Toast.LENGTH_LONG).show();
	        	finish();
	        }
	    }
		
	}


	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		uri = (Uri)savedInstanceState.getParcelable("uri");
		mPath = savedInstanceState.getString("path");
		mOPath = savedInstanceState.getString("opath");
		
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("uri", uri);
		outState.putString("path", mPath);
		outState.putString("opath", mOPath);
	}
	
	
}
