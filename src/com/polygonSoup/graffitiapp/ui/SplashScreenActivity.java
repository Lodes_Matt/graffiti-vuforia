package com.polygonSoup.graffitiapp.ui;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import com.polygonSoup.graffitiapp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;


public class SplashScreenActivity extends Activity {
	
	private String mServerUrl = "http://haxxortheplanet.koerce.me/graffiti/conncheck.php";

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		
		new ConnectionCheck().execute();
	}
	
	private class ConnectionCheck extends AsyncTask<Void, Void, Boolean> {
		

		@Override
		protected Boolean doInBackground(Void... params) {
			HttpClient http = new DefaultHttpClient();
			HttpGet get = new HttpGet(mServerUrl);
			ResponseHandler<String> rh = new BasicResponseHandler();
			String response;
			try {
				response = http.execute(get, rh);
				if(response.equals("hello")){
					return true;
				} else{
					return false;
				}
			
			} catch (ClientProtocolException e) {
				return false;
			} catch (IOException e) {
				return false;
			}
		}
		

		@Override
		protected void onPostExecute(Boolean result){
			if(!result){
				Toast.makeText(getApplicationContext(), R.string.noconn, Toast.LENGTH_SHORT).show();
			}
			Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
			i.putExtra("connection", result);
			startActivity(i);
			finish();
		}
	}
}
