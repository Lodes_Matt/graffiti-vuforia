/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_vuforia_samples_CylinderTargets_CylinderTargetsActivity */

#ifndef _Included_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets
#define _Included_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_initApplicationNative(
		JNIEnv *, jobject, jint width, jint height);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_onQCARInitializedNative(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_initTracker(
		JNIEnv *, jobject);

JNIEXPORT jint JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_loadTrackerData(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_startCamera(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_stopCamera(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_destroyTrackerData(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_deinitTracker(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_deinitApplicationNative(
		JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_updateRendering(
		JNIEnv *, jobject, jint width, jint height);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_initRendering
  (JNIEnv *, jobject);

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_renderFrame
  (JNIEnv *, jobject);

JNIEXPORT int JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_getOpenGlEsVersionNative
  (JNIEnv *, jobject);

JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_setActivityPortraitMode(JNIEnv *, jobject, jboolean isPortrait);

JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_autofocus(JNIEnv*, jobject);

JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_setFocusMode(JNIEnv*, jobject, jint mode);

#ifdef __cplusplus
}
#endif
#endif
