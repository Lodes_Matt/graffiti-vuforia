/*==============================================================================
    Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary

@file
    CylinderActivity.cpp

@brief

==============================================================================*/
#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>

#ifdef USE_OPENGL_ES_1_1
#include <GLES/gl.h>
#include <GLES/glext.h>
#else
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#include <QCAR/QCAR.h>
#include <QCAR/CameraDevice.h>
#include <QCAR/Renderer.h>
#include <QCAR/VideoBackgroundConfig.h>
#include <QCAR/Trackable.h>
#include <QCAR/TrackableResult.h>
#include <QCAR/CylinderTargetResult.h>
#include <QCAR/CylinderTarget.h>
#include <QCAR/Tool.h>
#include <QCAR/Tracker.h>
#include <QCAR/TrackerManager.h>
#include <QCAR/ImageTracker.h>
#include <QCAR/CameraCalibration.h>
#include <QCAR/UpdateCallback.h>
#include <QCAR/DataSet.h>

// Includes:
#include "SampleUtils.h"
#include "Texture.h"

#include "cylinder/CubeShaders.h"

#include "cylinder/CylinderTargets.h"
#include "cylinder/CylinderModel.h"
//#include "cylinder/soccerball.h"
#include "cylinder/sphere.h"

#ifdef __cplusplus
extern "C" {
#endif

class CylinderActivityApplication: public QCAR::UpdateCallback {
public:
	CylinderActivityApplication();

	virtual void QCAR_onUpdate(QCAR::State& state);

	int loadTrackerData();

	int destroyTrackerData();

	void GLInitRendering();

	void GLRenderFrame();

	void stopObjectAnimation();

	void startObjectAnimation();

	void moveObjectAway();

	void putObjectOnTarget();


private:
	// OpenGL ES 2.0 specific:
	unsigned int shaderProgramID;
	GLint vertexHandle;
	GLint normalHandle;
	GLint textureCoordHandle;
	GLint mvpMatrixHandle;
	GLint texSampler2DHandle;

	QCAR::DataSet* dataSet;

	CylinderModel mCylinderModel;
};

// dimensions of the cylinder (as set in the TMS tool)
const float kCylinderHeight = 95.0f; // 125.0f;
const float kCylinderTopDiameter = 65.0f; // 85.0f;
const float kCylinderBottomDiameter = 65.0f; // 60.0f;

// ratio between top and bottom diameter
// used to generate the model of the cylinder
const float kCylinderTopRadiusRatio = kCylinderTopDiameter / kCylinderBottomDiameter;

// the height of the cylinder model
const float kCanHeight = 1.0f;

// 1/3 of the height of the cylinder
const float kRatioCylinderHeight = 3.0f;

// Scaling of the cylinder model to match the ratio we want
const float kObjectScale = kCylinderHeight / (kRatioCylinderHeight * kCanHeight);

// scaling of the cylinder model to fit the actual cylinder
const float kCylinderScaleX    = kCylinderBottomDiameter / 2.0;
const float kCylinderScaleY    = kCylinderBottomDiameter / 2.0;
const float kCylinderScaleZ    = kCylinderHeight;

// The projection matrix used for rendering virtual objects:
QCAR::Matrix44F projectionMatrix;

// Textures:
int textureCount                = 0;
Texture** textures              = 0;

CylinderActivityApplication::CylinderActivityApplication():mCylinderModel(kCylinderTopRadiusRatio)
{
}

void CylinderActivityApplication::QCAR_onUpdate(QCAR::State& state) {
}

int CylinderActivityApplication::loadTrackerData() {
	// Get the image tracker:
	QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
	QCAR::ImageTracker* imageTracker =
			static_cast<QCAR::ImageTracker*>(trackerManager.getTracker(
					QCAR::Tracker::IMAGE_TRACKER));
	if (imageTracker == NULL) {
		LOG("Failed to load tracking data set because the ImageTracker has not been initialized.");
		return 0;
	}

	// Create the data sets:
	dataSet = imageTracker->createDataSet();
	if (dataSet == 0) {
		LOG("Failed to create a new tracking data.");
		return 0;
	}

	char * targetFileName = "sodacan.xml";

	// Load the data sets:
	if (!dataSet->load(targetFileName,
			QCAR::DataSet::STORAGE_APPRESOURCE)) {
		LOG("Failed to load data set.");
		return 0;
	}

	// Activate the data set:
	if (!imageTracker->activateDataSet(dataSet)) {
		LOG("Failed to activate data set.");
		return 0;
	}

	LOG("Successfully loaded and activated data set.");
	return 1;
}

int CylinderActivityApplication::destroyTrackerData() {
	// Get the image tracker:
	QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
	QCAR::ImageTracker* imageTracker =
			static_cast<QCAR::ImageTracker*>(trackerManager.getTracker(
					QCAR::Tracker::IMAGE_TRACKER));
	if (imageTracker == NULL) {
		LOG(
				"Failed to destroy the tracking data set because the ImageTracker has not"
				" been initialized.");
		return 0;
	}

	if (dataSet != 0) {
		if (imageTracker->getActiveDataSet() == dataSet
				&& !imageTracker->deactivateDataSet(dataSet)) {
			LOG(
					"Failed to destroy the tracking data set StonesAndChips because the data set "
					"could not be deactivated.");
			return 0;
		}

		if (!imageTracker->destroyDataSet(dataSet)) {
			LOG("Failed to destroy the tracking data set StonesAndChips.");
			return 0;
		}

		LOG("Successfully destroyed the data set StonesAndChips.");
		dataSet = 0;
	}

	return 1;
}

void CylinderActivityApplication::GLInitRendering() {
    // Define clear color
    glClearColor(0.0f, 0.0f, 0.0f, QCAR::requiresAlpha() ? 0.0f : 1.0f);

    // Now generate the OpenGL texture objects and add settings
    for (int i = 0; i < textureCount; ++i)
    {
        glGenTextures(1, &(textures[i]->mTextureID));
        glBindTexture(GL_TEXTURE_2D, textures[i]->mTextureID);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textures[i]->mWidth,
                textures[i]->mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                (GLvoid*)  textures[i]->mData);
    }
    SampleUtils::checkGlError("CylinderTargets GLInitRendering");

	shaderProgramID = SampleUtils::createProgramFromBuffer(
			cubeMeshVertexShader, cubeFragmentShader);
	SampleUtils::checkGlError("GLInitRendering");
	vertexHandle = glGetAttribLocation(shaderProgramID, "vertexPosition");
	normalHandle = glGetAttribLocation(shaderProgramID, "vertexNormal");
	textureCoordHandle = glGetAttribLocation(shaderProgramID, "vertexTexCoord");
	mvpMatrixHandle = glGetUniformLocation(shaderProgramID,
			"modelViewProjectionMatrix");
	texSampler2DHandle = glGetUniformLocation(shaderProgramID, "texSampler2D");
	SampleUtils::checkGlError("GLInitRendering due");
	LOG("shprog> %d, v> %d, n> %d, t> %d, mvp> %d, tx> %d", shaderProgramID, vertexHandle, normalHandle, textureCoordHandle, mvpMatrixHandle, texSampler2DHandle);
    SampleUtils::checkGlError("CylinderTargets GLInitRendering getting location att and unif");
}

double
getCurrentTime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    double t = tv.tv_sec + tv.tv_usec/1000000.0;
    return t;
}

void
animateObject(QCAR::Matrix44F& modelViewMatrix)
{
    static float rotateBowlAngle = 0.0f;

    static double prevTime = getCurrentTime();
    double time = getCurrentTime();             // Get real time difference
    float dt = (float)(time-prevTime);          // from frame to frame

    rotateBowlAngle += dt * 180.0f/3.1415f;     // Animate angle based on time

    SampleUtils::rotatePoseMatrix(rotateBowlAngle, 0.0f, 0.0f, 1.0f,
                                  &modelViewMatrix.data[0]);

    prevTime = time;
}

void CylinderActivityApplication::GLRenderFrame() {

	// Clear color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Get the state from QCAR and mark the beginning of a rendering section
	QCAR::State state = QCAR::Renderer::getInstance().begin();

	// Explicitly render the Video Background
	QCAR::Renderer::getInstance().drawVideoBackground();

	glEnable (GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    SampleUtils::checkGlError("CylinderTargets drawVideoBackground");

	// We must detect if background reflection is active and adjust the culling direction.
	// If the reflection is active, this means the post matrix has been reflected as well,
	// therefore standard counter clockwise face culling will result in "inside out" models.
	glEnable (GL_CULL_FACE);
	glCullFace (GL_BACK);
	if (QCAR::Renderer::getInstance().getVideoBackgroundConfig().mReflection
			== QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
		glFrontFace (GL_CW);  //Front camera
	else
		glFrontFace (GL_CCW);   //Back camera

	// Did we find any trackables this frame?
	for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
		// Get the trackable:
		const QCAR::TrackableResult* result = state.getTrackableResult(tIdx);

		// only deal with cylinder
		if (result->getType() != QCAR::TrackableResult::CYLINDER_TARGET_RESULT)
		{
			continue;
		}
		const QCAR::CylinderTargetResult * cylinderResult = (QCAR::CylinderTargetResult *) result;
		const QCAR::CylinderTarget& cylinderTarget = cylinderResult->getTrackable();

		//const QCAR::Trackable& trackable = result->getTrackable();
		QCAR::Matrix44F modelViewMatrix;
        QCAR::Matrix44F modelViewProjection;

		// texture for object
        const Texture* const thisTexture = textures[1];

#ifdef TEST_ORIGINAL
		VuforiaUtils::translatePoseMatrix(0.0f, 0.0f, kObjectScale,
				&modelViewMatrix.data[0]);
		VuforiaUtils::scalePoseMatrix(kObjectScale, kObjectScale, kObjectScale,
				&modelViewMatrix.data[0]);
		VuforiaUtils::multiplyMatrix(&appContext->projectionMatrix.data[0],
				&modelViewMatrix.data[0], &modelViewProjection.data[0]);
#endif


		// prepare the cylinder
        modelViewMatrix = QCAR::Tool::convertPose2GLMatrix(result->getPose());

//        VuforiaUtils::rotatePoseMatrix(90, 1.0f, 0.0f, 0.0f,
//                                      &modelViewMatrix.data[0]);

        SampleUtils::scalePoseMatrix(kCylinderScaleX, kCylinderScaleY, kCylinderScaleZ,
                                     &modelViewMatrix.data[0]);
        SampleUtils::multiplyMatrix(&projectionMatrix.data[0],
                                    &modelViewMatrix.data[0],
                                    &modelViewProjection.data[0]);
        SampleUtils::checkGlError("CylinderTargets prepareCylinder");

        glUseProgram(shaderProgramID);

        // Draw the cylinder:

        // We must detect if background reflection is active and adjust the culling direction.
        // If the reflection is active, this means the post matrix has been reflected as well,
        // therefore standard counter clockwise face culling will result in "inside out" models.
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        if(QCAR::Renderer::getInstance().getVideoBackgroundConfig().mReflection == QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
            glFrontFace(GL_CW);  //Front camera
        else
            glFrontFace(GL_CCW);   //Back camera

        glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0,
                              (const GLvoid*) mCylinderModel.ptrVertices());
        glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0,
                              (const GLvoid*) mCylinderModel.ptrNormals());
        glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0,
                              (const GLvoid*) mCylinderModel.ptrTexCoords());

        glEnableVertexAttribArray(vertexHandle);
        glEnableVertexAttribArray(normalHandle);
        glEnableVertexAttribArray(textureCoordHandle);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textures[0]->mTextureID);
        glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE,
                           (GLfloat*)&modelViewProjection.data[0] );
        glUniform1i(texSampler2DHandle, 0 /*GL_TEXTURE0*/);
        glDrawElements(GL_TRIANGLES, mCylinderModel.nbIndices(), GL_UNSIGNED_SHORT,
                       (const GLvoid*) mCylinderModel.ptrIndices());

        glDisable(GL_CULL_FACE);
        SampleUtils::checkGlError("CylinderTargets drawCylinder");





        // prepare the object
        modelViewMatrix = QCAR::Tool::convertPose2GLMatrix(result->getPose());

		// draw the anchored object
		animateObject(modelViewMatrix);

		// we move away the object from the target
		SampleUtils::translatePoseMatrix(1.0f * kCylinderTopDiameter, 0.0f,  kObjectScale,
				&modelViewMatrix.data[0]);
		SampleUtils::scalePoseMatrix(kObjectScale, kObjectScale, kObjectScale,
				&modelViewMatrix.data[0]);


		SampleUtils::multiplyMatrix(&projectionMatrix.data[0],
				&modelViewMatrix.data[0], &modelViewProjection.data[0]);

		glUseProgram (shaderProgramID);

		glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0,
				(const GLvoid*) &sphereVerts[0]);
		glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0,
				(const GLvoid*) &sphereNormals[0]);
		glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0,
				(const GLvoid*) &sphereTexCoords[0]);

		glActiveTexture (GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, thisTexture->mTextureID);
		glUniform1i(texSampler2DHandle, 0 /*GL_TEXTURE0*/);
		glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE,
				(GLfloat*) &modelViewProjection.data[0]);
		glDrawArrays(GL_TRIANGLES, 0, sphereNumVerts);

		glDisableVertexAttribArray (vertexHandle);
		glDisableVertexAttribArray (normalHandle);
		glDisableVertexAttribArray (textureCoordHandle);

		SampleUtils::checkGlError("CylinderTargets renderFrame");
	}

    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);

	QCAR::Renderer::getInstance().end();
}

///////////////////////////////////////////////////////////////////////////////////////
static CylinderActivityApplication * application;

// Indicates whether screen is in portrait (true) or landscape (false) mode
bool isActivityInPortraitMode   = false;

// Screen dimensions:
unsigned int screenWidth        = 0;
unsigned int screenHeight       = 0;

void
configureVideoBackground()
{
    // Get the default video mode:
    QCAR::CameraDevice& cameraDevice = QCAR::CameraDevice::getInstance();
    QCAR::VideoMode videoMode = cameraDevice.
                                getVideoMode(QCAR::CameraDevice::MODE_DEFAULT);


    // Configure the video background
    QCAR::VideoBackgroundConfig config;
    config.mEnabled = true;
    config.mSynchronous = true;
    config.mPosition.data[0] = 0.0f;
    config.mPosition.data[1] = 0.0f;

    if (isActivityInPortraitMode)
    {
        config.mSize.data[0] = videoMode.mHeight
                                * (screenHeight / (float)videoMode.mWidth);
        config.mSize.data[1] = screenHeight;

        if(config.mSize.data[0] < screenWidth)
        {
            LOG("Correcting rendering background size to handle missmatch between screen and video aspect ratios.");
            config.mSize.data[0] = screenWidth;
            config.mSize.data[1] = screenWidth *
                              (videoMode.mWidth / (float)videoMode.mHeight);
        }
    }
    else
    {
        config.mSize.data[0] = screenWidth;
        config.mSize.data[1] = videoMode.mHeight
                            * (screenWidth / (float)videoMode.mWidth);

        if(config.mSize.data[1] < screenHeight)
        {
            LOG("Correcting rendering background size to handle missmatch between screen and video aspect ratios.");
            config.mSize.data[0] = screenHeight
                                * (videoMode.mWidth / (float)videoMode.mHeight);
            config.mSize.data[1] = screenHeight;
        }
    }

    LOG("Configure Video Background : Video (%d,%d), Screen (%d,%d), mSize (%d,%d)", videoMode.mWidth, videoMode.mHeight, screenWidth, screenHeight, config.mSize.data[0], config.mSize.data[1]);

    // Set the config:
    QCAR::Renderer::getInstance().setVideoBackgroundConfig(config);
}

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_initRendering
(JNIEnv *, jobject) {
	if (application) {
		application->GLInitRendering();
	}
}

JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_renderFrame
(JNIEnv *, jobject) {
	if (application) {
		application->GLRenderFrame();
	}
}

// called when the AR application is created
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_initApplicationNative(
		JNIEnv * env, jobject obj, jint width, jint height) {
	// Screen dimensions:
	screenWidth        = width;
	screenHeight       = height;

	application = new CylinderActivityApplication();

    // Handle to the activity class:
    jclass activityClass = env->GetObjectClass(obj);

    jmethodID getTextureCountMethodID = env->GetMethodID(activityClass,
                                                    "getTextureCount", "()I");
    if (getTextureCountMethodID == 0)
    {
        LOG("Function getTextureCount() not found.");
        return;
    }

    textureCount = env->CallIntMethod(obj, getTextureCountMethodID);
    if (!textureCount)
    {
        LOG("getTextureCount() returned zero.");
        return;
    }

    textures = new Texture*[textureCount];

    jmethodID getTextureMethodID = env->GetMethodID(activityClass,
        "getTexture", "(I)Lcom/qualcomm/QCARSamples/CylinderTargets/Texture;");

    if (getTextureMethodID == 0)
    {
        LOG("Function getTexture() not found.");
        return;
    }

    // Register the textures
    for (int i = 0; i < textureCount; ++i)
    {

        jobject textureObject = env->CallObjectMethod(obj, getTextureMethodID, i);
        if (textureObject == NULL)
        {
            LOG("GetTexture() returned zero pointer");
            return;
        }

        textures[i] = Texture::create(env, textureObject);
    }
    LOG("Java_com_qualcomm_QCARSamples_ImageTargets_ImageTargets_initApplicationNative finished");

}

// call for the application to register its Vuforia callback
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_onQCARInitializedNative(
		JNIEnv *, jobject) {
	if (application) {
		QCAR::registerCallback(application);
	}
}

// time to initialize the Vuforia tracker(s)
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_initTracker(
		JNIEnv *, jobject) {
	// Initialize the image tracker:
	QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
	QCAR::Tracker* tracker = trackerManager.initTracker(
			QCAR::Tracker::IMAGE_TRACKER);
	if (tracker == NULL) {
		LOG("Failed to initialize ImageTracker.");
	} else {
		LOG("Successfully initialized ImageTracker.");
	}
}

// time to load the tracker data set(s)
JNIEXPORT jint JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_loadTrackerData(
		JNIEnv *, jobject) {
	int result = 0;

	result = application->loadTrackerData();

	return result;

}

// called when the camera is running
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_startCamera(
		JNIEnv *, jobject) {

    // Select the camera to open, set this to QCAR::CameraDevice::CAMERA_FRONT
    // to activate the front camera instead.
    QCAR::CameraDevice::CAMERA camera = QCAR::CameraDevice::CAMERA_DEFAULT;

    // Initialize the camera:
    if (!QCAR::CameraDevice::getInstance().init(camera))
        return;

    // Configure the video background
    configureVideoBackground();

    // Select the default mode:
    if (!QCAR::CameraDevice::getInstance().selectVideoMode(
                                QCAR::CameraDevice::MODE_DEFAULT))
        return;

    // Start the camera:
    if (!QCAR::CameraDevice::getInstance().start())
        return;

	// Start the tracker:
	QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
	QCAR::Tracker* imageTracker = trackerManager.getTracker(
			QCAR::Tracker::IMAGE_TRACKER);
	if (imageTracker != 0) {
		LOG("@@ imageTracker started");
		imageTracker->start();
	}

}

// called when the camera is stopped
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_stopCamera(
		JNIEnv *, jobject) {
	QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
	QCAR::Tracker* imageTracker = trackerManager.getTracker(
			QCAR::Tracker::IMAGE_TRACKER);
	if (imageTracker != 0) {
		imageTracker->stop();
	}

    QCAR::CameraDevice::getInstance().stop();
    QCAR::CameraDevice::getInstance().deinit();
}

// time to destroy the data set(s)
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_destroyTrackerData(
		JNIEnv *, jobject) {
	if (application) {
		application->destroyTrackerData();
	}
}

// time to deallocate the tracker(s)
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_deinitTracker(
		JNIEnv *, jobject) {
	// Deinit the image tracker:
	QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
	trackerManager.deinitTracker(QCAR::Tracker::IMAGE_TRACKER);
}
// called when the application is stopped
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_deinitApplicationNative(
		JNIEnv *, jobject) {

    // Release texture resources
    if (textures != 0)
    {
        for (int i = 0; i < textureCount; ++i)
        {
            delete textures[i];
            textures[i] = NULL;
        }

        delete[]textures;
        textures = NULL;

        textureCount = 0;
    }

	if (application) {
		delete application;
		application = NULL;
	}
}

// called when the state of the rendering has changed such as
// a screen change of orientation or when the camera just started
JNIEXPORT void JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_updateRendering(
		JNIEnv *, jobject, jint width, jint height) {

    LOG("Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargetsRenderer_updateRendering");

    // Update screen dimensions
    screenWidth = width;
    screenHeight = height;

    // Reconfigure the video background
    configureVideoBackground();

}

JNIEXPORT int JNICALL Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_getOpenGlEsVersionNative(
		JNIEnv *, jobject)
{
    return 2;
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_setActivityPortraitMode(JNIEnv *, jobject, jboolean isPortrait)
{
    isActivityInPortraitMode = isPortrait;
}

// ----------------------------------------------------------------------------
// Activates Camera Flash
// ----------------------------------------------------------------------------
JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_activateFlash(JNIEnv*, jobject, jboolean flash)
{
    return QCAR::CameraDevice::getInstance().setFlashTorchMode((flash==JNI_TRUE)) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_autofocus(JNIEnv*, jobject)
{
    return QCAR::CameraDevice::getInstance().setFocusMode(QCAR::CameraDevice::FOCUS_MODE_TRIGGERAUTO) ? JNI_TRUE : JNI_FALSE;
}


JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_setFocusMode(JNIEnv*, jobject, jint mode)
{
    int qcarFocusMode;

    switch ((int)mode)
    {
        case 0:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_NORMAL;
            break;

        case 1:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_CONTINUOUSAUTO;
            break;

        case 2:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_INFINITY;
            break;

        case 3:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_MACRO;
            break;

        default:
            return JNI_FALSE;
    }

    return QCAR::CameraDevice::getInstance().setFocusMode(qcarFocusMode) ? JNI_TRUE : JNI_FALSE;
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_setProjectionMatrix(JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_CylinderTargets_CylinderTargets_setProjectionMatrix");

    // Cache the projection matrix:
    const QCAR::CameraCalibration& cameraCalibration =
                                QCAR::CameraDevice::getInstance().getCameraCalibration();
    projectionMatrix = QCAR::Tool::getProjectionGL(cameraCalibration, 2.0f, 2500.0f);
}


#ifdef __cplusplus
}
#endif
