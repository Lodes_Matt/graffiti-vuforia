/*==============================================================================
Copyright (c) 2011-2013 QUALCOMM Austria Research Center GmbH.
All Rights Reserved.

@file
    OcclusionManagement.cpp

@brief
    Sample for OcclusionManagement

==============================================================================*/


#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <QCAR/QCAR.h>
#include <QCAR/CameraDevice.h>
#include <QCAR/Renderer.h>
#include <QCAR/VideoBackgroundConfig.h>
#include <QCAR/Trackable.h>
#include <QCAR/TrackableResult.h>
#include <QCAR/Tool.h>
#include <QCAR/TrackerManager.h>
#include <QCAR/ImageTracker.h>
#include <QCAR/ImageTarget.h>
#include <QCAR/MultiTarget.h>
#include <QCAR/CameraCalibration.h>
#include <QCAR/DataSet.h>
#include <QCAR/VideoBackgroundTextureInfo.h>

#include "SampleUtils.h"
#include "Texture.h"
#include "CubeShaders.h"
#include "Shaders.h"
#include "Cube.h"
#include "Teapot.h"

#ifdef __cplusplus
extern "C"
{
#endif

// Textures:
int textureCount                = 0;
Texture** textures              = 0;

// OpenGL ES 2.0 specific:
unsigned int shaderProgramID    = 0;
GLint vertexHandle              = 0;
GLint normalHandle              = 0;
GLint textureCoordHandle        = 0;
GLint mvpMatrixHandle           = 0;

// These values will hold the GL viewport
int viewportPosition_x          = 0;
int viewportPosition_y          = 0;
int viewportSize_x              = 0;
int viewportSize_y              = 0;

float vbOrthoQuadVertices[] =
{
    -1.0f, -1.0f, 0.0f,
     1.0f, -1.0f, 0.0f,
     1.0f,  1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f
};

float vbOrthoQuadTexCoords[] =
{
    0.0,    1.0,
    1.0,    1.0,
    1.0,    0.0,
    0.0,    0.0
};

GLbyte vbOrthoQuadIndices[]=
{
    0, 1, 2, 2, 3, 0
};

float   vbOrthoProjMatrix[16];

unsigned int vbShaderProgramOcclusionID         = 0;
GLint vbVertexPositionOcclusionHandle           = 0;
GLint vbVertexTexCoordOcclusionHandle           = 0;
GLint vbTexSamplerVideoOcclusionHandle          = 0;
GLint vbProjectionMatrixOcclusionHandle         = 0;
GLint vbTexSamplerMaskOcclusionHandle           = 0;
GLint vbViewportOriginHandle                    = 0;
GLint vbViewportSizeHandle                      = 0;
GLint vbTextureRatioHandle                      = 0;
GLint vbPrefixHandle                            = 0;
GLint vbInversionMultiplierHandle               = 0;
GLint vbPortraitHandle                          = 0;

unsigned int vbShaderProgramID                  = 0;
GLint vbVertexPositionHandle                    = 0;
GLint vbVertexTexCoordHandle                    = 0;
GLint vbTexSamplerVideoHandle                   = 0;
GLint vbProjectionMatrixHandle                  = 0;

// Screen dimensions:
unsigned int screenWidth                        = 0;
unsigned int screenHeight                       = 0;

// Indicates whether screen is in portrait (true) or landscape (false) mode
bool isActivityInPortraitMode               = false;

// The projection matrix used for rendering virtual objects:
QCAR::Matrix44F projectionMatrix;

// Constants:
static const float kCubeScaleX              = 120.0f * 0.75f / 2.0f;
static const float kCubeScaleY              = 120.0f * 1.00f / 2.0f;
static const float kCubeScaleZ              = 120.0f * 0.50f / 2.0f;

static const float kTeapotScaleX            = 120.0f * 0.015f;
static const float kTeapotScaleY            = 120.0f * 0.015f;
static const float kTeapotScaleZ            = 120.0f * 0.015f;

QCAR::DataSet* dataSet                      = 0;


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_setActivityPortraitMode(
                                        JNIEnv *, jobject, jboolean isPortrait)
{
    isActivityInPortraitMode = isPortrait;
}


JNIEXPORT int JNICALL
    Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_initTracker(
                                        JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_initTracker");

    // Initialize the image tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::Tracker* tracker = trackerManager.initTracker
                                            (QCAR::Tracker::IMAGE_TRACKER);
    if (tracker == NULL)
    {
        LOG("Failed to initialize ImageTracker.");
        return 0;
    }

    LOG("Successfully initialized ImageTracker.");
    return 1;
}


JNIEXPORT void JNICALL
    Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_deinitTracker(
                                        JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_deinitTracker");

    // Deinit the image tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    trackerManager.deinitTracker(QCAR::Tracker::IMAGE_TRACKER);
}


JNIEXPORT int JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_loadTrackerData(
                                        JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_loadTrackerData");

    // Get the image tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* imageTracker = static_cast<QCAR::ImageTracker*>(
                    trackerManager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    if (imageTracker == NULL)
    {
        LOG("Failed to load tracking data set because the ImageTracker has not"
            " been initialized.");
        return 0;
    }

    // Create the data set:
    dataSet = imageTracker->createDataSet();
    if (dataSet == 0)
    {
        LOG("Failed to create a new tracking data.");
        return 0;
    }

    // Load the data set:
    if (!dataSet->load("FlakesBox.xml", QCAR::DataSet::STORAGE_APPRESOURCE))
    {
        LOG("Failed to load data set.");
        return 0;
    }

    LOG("Successfully loaded the data set.");
    return 1;
}


JNIEXPORT int JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_destroyTrackerData(
                                        JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_destroyTrackerData");

    // Get the image tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* imageTracker = static_cast<QCAR::ImageTracker*>(
        trackerManager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    if (imageTracker == NULL)
    {
        LOG("Failed to destroy the tracking data set because the ImageTracker"
            " has not been initialized.");
        return 0;
    }

    if (dataSet != 0)
    {
        if (!imageTracker->deactivateDataSet(dataSet))
        {
            LOG("Failed to destroy the tracking data set because the data set "
                "could not be deactivated.");
            return 0;
        }

        if (!imageTracker->destroyDataSet(dataSet))
        {
            LOG("Failed to destroy the tracking data set.");
            return 0;
        }

        LOG("Successfully destroyed the data set.");
        dataSet = 0;
        return 1;
    }

    LOG("No tracker data set to destroy.");
    return 0;
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_onQCARInitializedNative(
                                JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_onQCARInitializedNative");

    // Get the image tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* imageTracker = static_cast<QCAR::ImageTracker*>(
        trackerManager.getTracker(QCAR::Tracker::IMAGE_TRACKER));

    // Activate the data set:
    if (!imageTracker->activateDataSet(dataSet))
    {
        LOG("Failed to activate data set.");
        return;
    }

    LOG("Successfully activated the data set.");
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagementRenderer_renderFrame(
                                JNIEnv *, jobject)
{
    //LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_GLRenderer_renderFrame");
    SampleUtils::checkGlError("Check gl errors prior render Frame");

    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Get the state from QCAR and mark the beginning of a rendering section
    QCAR::State state = QCAR::Renderer::getInstance().begin();

    const QCAR::VIDEO_BACKGROUND_REFLECTION reflection = QCAR::Renderer::getInstance().getVideoBackgroundConfig().mReflection;


    const QCAR::VideoBackgroundTextureInfo texInfo =
                QCAR::Renderer::getInstance().getVideoBackgroundTextureInfo();
    float uRatio =
        ((float)texInfo.mImageSize.data[0]/(float)texInfo.mTextureSize.data[0]);
    float vRatio =
        ((float)texInfo.mImageSize.data[1]/(float)texInfo.mTextureSize.data[1]);


    // The following are the texture coordinates necessary for the correct mapping
    // of the PORTRAIT/LANDSCAPE and Reflection ON/OFF combinations for the
    // VideoBackground as well as the coefficients for the occlusion shader
    float inversion_multiplier[2];
    float prefix[2];
    if (isActivityInPortraitMode)
    {
        if(reflection == QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
        {
            // Portrait Reflection On
            //
            
            // VideoBackground Texture Coordinates
            vbOrthoQuadTexCoords[0] = 0;        vbOrthoQuadTexCoords[1] = vRatio;
            vbOrthoQuadTexCoords[2] = 0;        vbOrthoQuadTexCoords[3] = 0;
            vbOrthoQuadTexCoords[4] = uRatio;   vbOrthoQuadTexCoords[5] = 0;
            vbOrthoQuadTexCoords[6] = uRatio;   vbOrthoQuadTexCoords[7] = vRatio;

            // Fragment shader coefficents            
            prefix[0] = 0.0;
            prefix[1] = 1.0;
            inversion_multiplier[0] =  1.0;
            inversion_multiplier[1] = -1.0;
        }
        else 
        {
            // Portrait Reflection Off
            //
            
            // VideoBackground Texture Coordinates
            vbOrthoQuadTexCoords[0] = uRatio;   vbOrthoQuadTexCoords[1] = vRatio;
            vbOrthoQuadTexCoords[2] = uRatio;   vbOrthoQuadTexCoords[3] = 0;
            vbOrthoQuadTexCoords[4] = 0;        vbOrthoQuadTexCoords[5] = 0;
            vbOrthoQuadTexCoords[6] = 0;        vbOrthoQuadTexCoords[7] = vRatio;
            
            // Fragment shader coefficents            
            prefix[0] = 1.0;
            prefix[1] = 1.0;
            inversion_multiplier[0] = -1.0;
            inversion_multiplier[1] = -1.0;
        }
    }
    else
    {
        // If we detect a reflection, invert the x coords for the video background texture
        if(reflection == QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
        {
            // Landscape Reflection On
            //
            
            // VideoBackground Texture Coordinates
            vbOrthoQuadTexCoords[0] = uRatio;   vbOrthoQuadTexCoords[1] = vRatio;
            vbOrthoQuadTexCoords[2] = 0;        vbOrthoQuadTexCoords[3] = vRatio;
            vbOrthoQuadTexCoords[4] = 0;        vbOrthoQuadTexCoords[5] = 0;
            vbOrthoQuadTexCoords[6] = uRatio;   vbOrthoQuadTexCoords[7] = 0;
            
            // Fragment shader coefficents            
            prefix[0] = 1.0;
            prefix[1] = 1.0;
            inversion_multiplier[0] = -1.0;
            inversion_multiplier[1] = -1.0;
        }
        else 
        {
            // Landscape Reflection Off
            //
            
            // VideoBackground Texture Coordinates
            vbOrthoQuadTexCoords[0] = 0;        vbOrthoQuadTexCoords[1] = vRatio;
            vbOrthoQuadTexCoords[2] = uRatio;   vbOrthoQuadTexCoords[3] = vRatio;
            vbOrthoQuadTexCoords[4] = uRatio;   vbOrthoQuadTexCoords[5] = 0;
            vbOrthoQuadTexCoords[6] = 0;        vbOrthoQuadTexCoords[7] = 0;
            
            // Fragment shader coefficents            
            prefix[0] = 0.0;
            prefix[1] = 1.0;
            inversion_multiplier[0] =  1.0;
            inversion_multiplier[1] = -1.0;
        }
    }


    ////////////////////////////////////////////////////////////////////////////
    // This section renders the video background with a
    // a shader defined in Shaders.h, it doesn't apply any effect, it merely
    // renders it
    // 
    GLuint vbVideoTextureUnit = 0;
    GLuint vbMaskTextureUnit = 1;
    QCAR::Renderer::getInstance().bindVideoBackground(vbVideoTextureUnit);
    
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // Set the viewport
    glViewport(viewportPosition_x, viewportPosition_y, viewportSize_x, viewportSize_y);

    // Load the shader and upload the vertex/texcoord/index data
    glUseProgram(vbShaderProgramID);
    glVertexAttribPointer(vbVertexPositionHandle, 3, GL_FLOAT, GL_FALSE, 0,
                                                        vbOrthoQuadVertices);
    glVertexAttribPointer(vbVertexTexCoordHandle, 2, GL_FLOAT, GL_FALSE, 0,
                                                        vbOrthoQuadTexCoords);
    glUniform1i(vbTexSamplerVideoHandle, vbVideoTextureUnit);
    glUniformMatrix4fv(vbProjectionMatrixHandle, 1, GL_FALSE,
                                                        &vbOrthoProjMatrix[0]);

    // Render the video background 
    glEnableVertexAttribArray(vbVertexPositionHandle);
    glEnableVertexAttribArray(vbVertexTexCoordHandle);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, vbOrthoQuadIndices);
    glDisableVertexAttribArray(vbVertexPositionHandle);
    glDisableVertexAttribArray(vbVertexTexCoordHandle);

    // Wrap up this rendering
    glUseProgram(0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    SampleUtils::checkGlError("Rendering of the video background");
    //
    ////////////////////////////////////////////////////////////////////////////


    
    ////////////////////////////////////////////////////////////////////////////
    // These OpenGL setup calls are important for a proper blending
    //
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //
    ////////////////////////////////////////////////////////////////////////////

    // Did we find any trackables this frame?
    if (state.getNumTrackableResults())
    {
        // Get the trackable:
        const QCAR::TrackableResult* result=NULL;
        int numResults=state.getNumTrackableResults();

        // Browse results searching for the MultiTarget
        for (int j=0;j<numResults;j++)
        {
            result = state.getTrackableResult(j);
            if (result->getType() == QCAR::TrackableResult::MULTI_TARGET_RESULT) break;
            result=NULL;
        }

        // If it was not found exit
        if (result==NULL)
        {
            // Clean up and leave
            glDisable(GL_BLEND);
            glDisable(GL_DEPTH_TEST);

            QCAR::Renderer::getInstance().end();
            return;
        }

        QCAR::Matrix44F modelViewMatrix =
                        QCAR::Tool::convertPose2GLMatrix(result->getPose());
        QCAR::Matrix44F modelViewProjectionCube;
        QCAR::Matrix44F modelViewProjectionTeapot;

        SampleUtils::scalePoseMatrix(kCubeScaleX, kCubeScaleY,
                                    kCubeScaleZ, &modelViewMatrix.data[0]);
        SampleUtils::multiplyMatrix(&projectionMatrix.data[0],
                                    &modelViewMatrix.data[0],
                                    &modelViewProjectionCube.data[0]);

        ////////////////////////////////////////////////////////////////////////
        // First, we render the faces that serve as a "background" to the teapot
        // with a checkerboard texture. This helps the user to have a visually 
        // constrained space (otherwise the teapot looks floating in space)
        //
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        if(reflection == QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
            glFrontFace(GL_CW);  //Front camera
        else
            glFrontFace(GL_CCW);   //Back camera 

            glUseProgram(shaderProgramID);
            glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0,
                                            (const GLvoid*) &cubeVertices[0]);
            glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0,
                                            (const GLvoid*) &cubeNormals[0]);
            glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0,
                                            (const GLvoid*) &cubeTexCoords[0]);
            glEnableVertexAttribArray(vertexHandle);
            glEnableVertexAttribArray(normalHandle);
            glEnableVertexAttribArray(textureCoordHandle);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, textures[0]->mTextureID);
            glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE,
                                (GLfloat*)&modelViewProjectionCube.data[0] );
            glDrawElements(GL_TRIANGLES, NUM_CUBE_INDEX, GL_UNSIGNED_SHORT,
                                            (const GLvoid*) &cubeIndices[0]);
            
        glCullFace(GL_BACK);

        SampleUtils::checkGlError("Back faces of the box");
        //
        ////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////
        // Then, we render the actual teapot
        //
        modelViewMatrix = QCAR::Tool::convertPose2GLMatrix(result->getPose());
        SampleUtils::translatePoseMatrix(0.0f*120.0f, -0.0f*120.0f,
                                    -0.17f*120.0f, &modelViewMatrix.data[0]);
        SampleUtils::rotatePoseMatrix(90.0f, 0.0f, 0, 1,
                                    &modelViewMatrix.data[0]);
        SampleUtils::scalePoseMatrix(kTeapotScaleX, kTeapotScaleY, kTeapotScaleZ,
                                    &modelViewMatrix.data[0]);
        SampleUtils::multiplyMatrix(&projectionMatrix.data[0],
                                    &modelViewMatrix.data[0],
                                    &modelViewProjectionTeapot.data[0]);
        glUseProgram(shaderProgramID);
        glEnableVertexAttribArray(vertexHandle);
        glEnableVertexAttribArray(normalHandle);
        glEnableVertexAttribArray(textureCoordHandle);
        glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0,
                                    (const GLvoid*) &teapotVertices[0]);
        glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0,
                                    (const GLvoid*) &teapotNormals[0]);
        glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0,
                                    (const GLvoid*) &teapotTexCoords[0]);
        glBindTexture(GL_TEXTURE_2D, textures[1]->mTextureID);
        glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE,
                                (GLfloat*)&modelViewProjectionTeapot.data[0] );
        glDrawElements(GL_TRIANGLES, NUM_TEAPOT_OBJECT_INDEX, GL_UNSIGNED_SHORT,
                                    (const GLvoid*) &teapotIndices[0]);
        glBindTexture(GL_TEXTURE_2D, 0);
        //
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // Finally, we render the top layer based on the video image
        // this is the layer that actually gives the "transparent look"
        // notice that we use the mask.png (textures[2]->mTextureID)
        // to define how the transparency looks, you can play around with that
        // texture to change the effects.
        //
        glDepthFunc(GL_LEQUAL);
        QCAR::Renderer::getInstance().bindVideoBackground(vbVideoTextureUnit);
        glActiveTexture(GL_TEXTURE0 + vbMaskTextureUnit);
        glBindTexture(GL_TEXTURE_2D, textures[2]->mTextureID);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glViewport(viewportPosition_x, viewportPosition_y,
                   viewportSize_x, viewportSize_y);

                   
        glUseProgram(vbShaderProgramOcclusionID);
            glVertexAttribPointer(vbVertexPositionOcclusionHandle, 3, GL_FLOAT,
                                GL_FALSE, 0, (const GLvoid*) &cubeVertices[0]);
            glVertexAttribPointer(vbVertexTexCoordOcclusionHandle, 2, GL_FLOAT,
                                GL_FALSE, 0, (const GLvoid*) &cubeTexCoords[0]);
            glEnableVertexAttribArray(vbVertexPositionOcclusionHandle);
            glEnableVertexAttribArray(vbVertexTexCoordOcclusionHandle);

            glUniform2f(vbViewportOriginHandle,
                        viewportPosition_x, viewportPosition_y);
            glUniform2f(vbViewportSizeHandle, viewportSize_x, viewportSize_y);
            glUniform2f(vbTextureRatioHandle, uRatio, vRatio);
            glUniform2f(vbPrefixHandle, prefix[0], prefix[1]);
            glUniform2f(vbInversionMultiplierHandle, inversion_multiplier[0], inversion_multiplier[1]);
            glUniform1i(vbPortraitHandle, isActivityInPortraitMode);
            glUniform1i(vbTexSamplerVideoOcclusionHandle, vbVideoTextureUnit);
            glUniform1i(vbTexSamplerMaskOcclusionHandle, vbMaskTextureUnit);
            glUniformMatrix4fv(vbProjectionMatrixOcclusionHandle, 1, GL_FALSE,
                                (GLfloat*)&modelViewProjectionCube.data[0] );
                glDrawElements(GL_TRIANGLES, NUM_CUBE_INDEX, GL_UNSIGNED_SHORT,
                                (const GLvoid*) &cubeIndices[0]);
            glDisableVertexAttribArray(vbVertexPositionOcclusionHandle);
            glDisableVertexAttribArray(vbVertexTexCoordOcclusionHandle);
        glUseProgram(0);
        
        
        glDisableVertexAttribArray(vertexHandle);
        glDisableVertexAttribArray(normalHandle);
        glDisableVertexAttribArray(textureCoordHandle);

        glDepthFunc(GL_LESS);
        SampleUtils::checkGlError("Transparency layer");
        //
        ////////////////////////////////////////////////////////////////////////
    }

    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);

    QCAR::Renderer::getInstance().end();
}


void
configureVideoBackground()
{
    // Get the default video mode:
    QCAR::CameraDevice& cameraDevice = QCAR::CameraDevice::getInstance();
    QCAR::VideoMode videoMode = cameraDevice.
                                getVideoMode(QCAR::CameraDevice::MODE_DEFAULT);

    // Configure the video background
    QCAR::VideoBackgroundConfig config;
    config.mEnabled = true;
    config.mSynchronous = true;
    config.mPosition.data[0] = 0.0f;
    config.mPosition.data[1] = 0.0f;

    if (isActivityInPortraitMode)
    {
        //LOG("configureVideoBackground PORTRAIT");
        config.mSize.data[0] = videoMode.mHeight
                                * (screenHeight / (float)videoMode.mWidth);
        config.mSize.data[1] = screenHeight;

        if(config.mSize.data[0] < screenWidth)
        {
            LOG("Correcting rendering background size to handle missmatch"
                " between screen and video aspect ratios.");
            config.mSize.data[0] = screenWidth;
            config.mSize.data[1] = screenWidth *
                              (videoMode.mWidth / (float)videoMode.mHeight);
        }
    }
    else
    {
        //LOG("configureVideoBackground LANDSCAPE");
        config.mSize.data[0] = screenWidth;
        config.mSize.data[1] = videoMode.mHeight
                            * (screenWidth / (float)videoMode.mWidth);

        if(config.mSize.data[1] < screenHeight)
        {
            LOG("Correcting rendering background size to handle missmatch"
                " between screen and video aspect ratios.");
            config.mSize.data[0] = screenHeight
                                * (videoMode.mWidth / (float)videoMode.mHeight);
            config.mSize.data[1] = screenHeight;
        }
    }

    LOG("Configure Video Background : Video (%d,%d), Screen (%d,%d), mSize (%d,%d)",
                        videoMode.mWidth, videoMode.mHeight, screenWidth,
                        screenHeight, config.mSize.data[0], config.mSize.data[1]);

    viewportPosition_x =  (((int)(screenWidth  - config.mSize.data[0])) / (int) 2) + 
                                                    config.mPosition.data[0];
    viewportPosition_y =  (((int)(screenHeight - config.mSize.data[1])) / (int) 2) + 
                                                    config.mPosition.data[1];
    viewportSize_x = config.mSize.data[0];
    viewportSize_y = config.mSize.data[1];

    LOG("Configure Video Background : Viewport (%d,%d,%d,%d)",
                            viewportPosition_x, viewportPosition_y,
                            viewportSize_x, viewportSize_y);

    // Set the config:
    QCAR::Renderer::getInstance().setVideoBackgroundConfig(config);
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_initApplicationNative(
                            JNIEnv* env, jobject obj, jint width, jint height)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_initApplicationNative");

    // Store screen dimensions
    screenWidth = width;
    screenHeight = height;

    // Handle to the activity class:
    jclass activityClass = env->GetObjectClass(obj);

    jmethodID getTextureCountMethodID = env->GetMethodID(activityClass,
                                                    "getTextureCount", "()I");
    if (getTextureCountMethodID == 0)
    {
        LOG("Function getTextureCount() not found.");
        return;
    }

    textureCount = env->CallIntMethod(obj, getTextureCountMethodID);
    if (!textureCount)
    {
        LOG("getTextureCount() returned zero.");
        return;
    }

    textures = new Texture*[textureCount];

    jmethodID getTextureMethodID = env->GetMethodID(activityClass,
        "getTexture", "(I)Lcom/qualcomm/QCARSamples/OcclusionManagement/Texture;");

    if (getTextureMethodID == 0)
    {
        LOG("Function getTexture() not found.");
        return;
    }

    // Register the textures
    for (int i = 0; i < textureCount; ++i)
    {

        jobject textureObject = env->CallObjectMethod(obj, getTextureMethodID, i);
        if (textureObject == NULL)
        {
            LOG("GetTexture() returned zero pointer");
            return;
        }

        textures[i] = Texture::create(env, textureObject);
    }
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_deinitApplicationNative(
                                                        JNIEnv* env, jobject obj)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_deinitApplicationNative");

    // Release texture resources
    if (textures != 0)
    {
        for (int i = 0; i < textureCount; ++i)
        {
            delete textures[i];
            textures[i] = NULL;
        }

        delete[]textures;
        textures = NULL;

        textureCount = 0;
    }
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_startCamera(JNIEnv *,
                                                                         jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_startCamera");

    // Select the camera to open, set this to QCAR::CameraDevice::CAMERA_FRONT 
    // to activate the front camera instead.
    QCAR::CameraDevice::CAMERA camera = QCAR::CameraDevice::CAMERA_DEFAULT;

    // Initialize the camera:
    if (!QCAR::CameraDevice::getInstance().init(camera))
        return;

    // Configure the video background
    configureVideoBackground();

    // Select the default mode:
    if (!QCAR::CameraDevice::getInstance().selectVideoMode(
                                QCAR::CameraDevice::MODE_DEFAULT))
        return;

    // Start the camera:
    if (!QCAR::CameraDevice::getInstance().start())
        return;

    // Start the tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::Tracker* imageTracker =
                        trackerManager.getTracker(QCAR::Tracker::IMAGE_TRACKER);
    if(imageTracker != 0)
        imageTracker->start();
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_stopCamera(JNIEnv *,
                                                                   jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_stopCamera");

    // Stop the tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::Tracker* imageTracker =
                        trackerManager.getTracker(QCAR::Tracker::IMAGE_TRACKER);
    if(imageTracker != 0)
        imageTracker->stop();

    QCAR::CameraDevice::getInstance().stop();
    QCAR::CameraDevice::getInstance().deinit();
}

JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_setProjectionMatrix(JNIEnv *, jobject)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_setProjectionMatrix");

    // Cache the projection matrix:
    const QCAR::CameraCalibration& cameraCalibration =
                                QCAR::CameraDevice::getInstance().getCameraCalibration();
    projectionMatrix = QCAR::Tool::getProjectionGL(cameraCalibration, 2.0f, 2500.0f);
}


JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_autofocus(JNIEnv*, jobject)
{
    return QCAR::CameraDevice::getInstance().setFocusMode(QCAR::CameraDevice::FOCUS_MODE_TRIGGERAUTO) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagement_setFocusMode
                                            (JNIEnv*, jobject, jint mode)
{
    int qcarFocusMode;

    switch ((int)mode)
    {
        case 0:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_NORMAL;
            break;
        
        case 1:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_CONTINUOUSAUTO;
            break;
            
        case 2:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_INFINITY;
            break;
            
        case 3:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_MACRO;
            break;
    
        default:
            return JNI_FALSE;
    }
    
    return QCAR::CameraDevice::getInstance().setFocusMode(qcarFocusMode) ? JNI_TRUE : JNI_FALSE;
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagementRenderer_initRendering(
                                                    JNIEnv* env, jobject obj)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagementRenderer_initRendering");

    // Define clear color
    glClearColor(0.0f, 0.0f, 0.0f, QCAR::requiresAlpha() ? 0.0f : 1.0f);

    // Now generate the OpenGL texture objects and add settings
    for (int i = 0; i < textureCount; ++i)
    {
        glGenTextures(1, &(textures[i]->mTextureID));
        glBindTexture(GL_TEXTURE_2D, textures[i]->mTextureID);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textures[i]->mWidth,
                textures[i]->mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                (GLvoid*)  textures[i]->mData);
    }

    shaderProgramID                     = SampleUtils::createProgramFromBuffer(
                            cubeMeshVertexShader, cubeFragmentShader);
    vertexHandle                        =
            glGetAttribLocation(shaderProgramID, "vertexPosition");
    normalHandle                        =
            glGetAttribLocation(shaderProgramID, "vertexNormal");
    textureCoordHandle                  =
            glGetAttribLocation(shaderProgramID, "vertexTexCoord");
    mvpMatrixHandle                     =
            glGetUniformLocation(shaderProgramID, "modelViewProjectionMatrix");


    vbShaderProgramID                   = SampleUtils::createProgramFromBuffer(
                            passThroughVertexShader, passThroughFragmentShader);
    vbVertexPositionHandle              =
            glGetAttribLocation(vbShaderProgramID, "vertexPosition");
    vbVertexTexCoordHandle              =
            glGetAttribLocation(vbShaderProgramID, "vertexTexCoord");
    vbProjectionMatrixHandle            =
            glGetUniformLocation(vbShaderProgramID, "modelViewProjectionMatrix");
    vbTexSamplerVideoHandle             =
            glGetUniformLocation(vbShaderProgramID, "texSamplerVideo");
    SampleUtils::setOrthoMatrix(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0,
                                                            vbOrthoProjMatrix);

    vbShaderProgramOcclusionID          = SampleUtils::createProgramFromBuffer(
                            passThroughVertexShader, occlusionFragmentShader);
    vbVertexPositionOcclusionHandle     =
            glGetAttribLocation(vbShaderProgramOcclusionID, "vertexPosition");
    vbVertexTexCoordOcclusionHandle     =
            glGetAttribLocation(vbShaderProgramOcclusionID, "vertexTexCoord");
    vbProjectionMatrixOcclusionHandle   =
            glGetUniformLocation(vbShaderProgramOcclusionID, "modelViewProjectionMatrix");
    vbViewportOriginHandle              =
            glGetUniformLocation(vbShaderProgramOcclusionID, "viewportOrigin");
    vbViewportSizeHandle                =
            glGetUniformLocation(vbShaderProgramOcclusionID, "viewportSize");
    vbTextureRatioHandle                =
            glGetUniformLocation(vbShaderProgramOcclusionID, "textureRatio");
    vbTexSamplerVideoOcclusionHandle    =
            glGetUniformLocation(vbShaderProgramOcclusionID, "texSamplerVideo");
    vbTexSamplerMaskOcclusionHandle     =
            glGetUniformLocation(vbShaderProgramOcclusionID, "texSamplerMask");
    vbPrefixHandle=
            glGetUniformLocation(vbShaderProgramOcclusionID, "prefix");
    vbInversionMultiplierHandle=
            glGetUniformLocation(vbShaderProgramOcclusionID, "inversion_multiplier");
    vbPortraitHandle=
            glGetUniformLocation(vbShaderProgramOcclusionID, "portrait");
}


JNIEXPORT void JNICALL
Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagementRenderer_updateRendering(
                        JNIEnv* env, jobject obj, jint width, jint height)
{
    LOG("Java_com_qualcomm_QCARSamples_OcclusionManagement_OcclusionManagementRenderer_updateRendering");

    // Update screen dimensions
    screenWidth = width;
    screenHeight = height;

    // Reconfigure the video background
    configureVideoBackground();
}


#ifdef __cplusplus
}
#endif
