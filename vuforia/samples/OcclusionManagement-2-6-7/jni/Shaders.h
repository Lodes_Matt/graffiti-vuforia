/*==============================================================================
Copyright (c) 2011-2013 QUALCOMM Austria Research Center GmbH.
All Rights Reserved.

==============================================================================*/

#define STRINGIFY(x) #x

static const char passThroughVertexShader[] = STRINGIFY(
    attribute vec4 vertexPosition;
    attribute vec2 vertexTexCoord;
    uniform mat4 modelViewProjectionMatrix;
    varying vec2 texCoord;

    void main()
    {
        gl_Position = modelViewProjectionMatrix * vertexPosition;
        texCoord = vertexTexCoord;
    }
);

static const char passThroughFragmentShader[] = STRINGIFY(
    precision mediump float;
    varying vec2 texCoord;
    uniform sampler2D texSamplerVideo;

    void main ()
    {
        gl_FragColor = texture2D(texSamplerVideo, texCoord);
    }
);

static const char occlusionFragmentShader[] = STRINGIFY(
    precision mediump float;
    varying vec2 texCoord;
    uniform sampler2D texSamplerMask;
    uniform sampler2D texSamplerVideo;
    uniform vec2 viewportOrigin;
    uniform vec2 viewportSize;
    uniform vec2 textureRatio;
    uniform vec2 prefix;
    uniform vec2 inversion_multiplier;
    uniform bool portrait;

    void main ()
    {
        vec2 screenCoord;
        float normalized_coordinates[2];

        // The following equations calculate the appropriate UV coordinates
        // to take from the video sampler. They consider whether the screen
        // is in landscape or portrait mode and whether it uses the front (reflected)
        // or back camera. The actual coefficients are passed by the main app.
        //
        normalized_coordinates[0] = (gl_FragCoord.x-viewportOrigin.x)/viewportSize.x;
        normalized_coordinates[1] = (gl_FragCoord.y-viewportOrigin.y)/viewportSize.y;
        
        if (portrait)
        {
            screenCoord.x = (prefix.x + (inversion_multiplier.x * normalized_coordinates[1])) * textureRatio.x;
            screenCoord.y = (prefix.y + (inversion_multiplier.y * normalized_coordinates[0])) * textureRatio.y;
        }
        else
        {
            screenCoord.x = (prefix.x + (inversion_multiplier.x * normalized_coordinates[0])) * textureRatio.x;
            screenCoord.y = (prefix.y + (inversion_multiplier.y * normalized_coordinates[1])) * textureRatio.y;
        }

        vec3 videoColor = texture2D(texSamplerVideo, screenCoord.xy).rgb;
        float maskColor  = texture2D(texSamplerMask, texCoord.xy).x;
        gl_FragColor.rgba = vec4(videoColor.r, videoColor.g, videoColor.b, maskColor);
    }
);
