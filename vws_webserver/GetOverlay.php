<?php


$name = $_GET['name'];

$dbconn = new mysqli('localhost', 'graffiti', 'haxxortheplanet', 'graffiti');
if($dbconn->connect_error){
	trigger_error('Database connection failed: '  . $dbconn->connect_error, E_USER_ERROR);		
}
$sqlq = 'SELECT overlay_path FROM data WHERE name=?';
$stmt = $dbconn->prepare($sqlq);
if($stmt === false) {
	trigger_error(' Error: ' . $dbconn->error, E_USER_ERROR);
}
$stmt->bind_param('s', $name);
$stmt->execute();
$stmt->bind_result($overlay_path);
while ($stmt->fetch()){
	echo(file_get_contents($overlay_path));
}
$stmt->close();
?>
