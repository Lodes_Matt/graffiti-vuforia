<?php

require_once 'HTTP/Request2.php';
require_once 'SignatureBuilder.php';

class PostNewTarget{

	//Server Keys
	//private $access_key 	= "9453ea9b5410639a43632c3150dbda9cc233b67b";
	//private $secret_key 	= "e75467652c074168cbd1e2568d2ab858681a8ccc";
	
	private $access_key 	= "52f82061e78ff88b0b3e077519b00e02d0b590c9";
	private $secret_key 	= "80c5cbc7e7f518b0841376145e695e8f427c3868";
	
	private $url 			= "https://vws.vuforia.com";
	private $requestPath 	= "/targets";
	private $request;       // the HTTP_Request2 object
	private $jsonRequestObject;
	private $Incomingjson;
	
	private $targetName 	= "[ name ]";
	private $width		= 320.0;
	private $active_flag	= 1;
	private $overlay = "[ overlay ]";
	private $overlay_name = "[ name ]";

	function PostNewTarget($json){
	  $this->incomingJson = json_decode($json);
	  $this->image = $this->incomingJson->{'image'};
	  $this->targetName = $this->incomingJson->{'name'};
	  $this->width = $this->incomingJson->{'width'};
	  $this->active_flag = $this->incomingJson->{'active_flag'};
	  $this->overlay = $this->incomingJson->{'overlay'}; 
	  $this->overlay_name = $this->incomingJson->{'overlay_name'};	
	  $this->jsonRequestObject = json_encode( array( 'width'=>$this->width, 'name'=>$this->targetName , 'image'=>$this->image, 'application_metadata'=>$this->overlay_name, 'active_flag'=>$this->active_flag ) );

	  $this->execPostNewTarget();

	  $this->execDatabaseUpdate();

	}
	function execDataBaseUpdate(){
	  //connect to database
	  $dbconn = new mysqli('localhost', 'graffiti', 'haxxortheplanet', 'graffiti');
	  if($dbconn->connect_error){
		trigger_error('Database connection failed: '  . $dbconn->connect_error, E_USER_ERROR);		
	  }
	  $sqlq = 'INSERT INTO data (name, image_path, overlay_path) VALUES (?,?,?)';
	  $stmt = $dbconn->prepare($sqlq);
	  if($stmt === false) {
  		trigger_error(' Error: ' . $dbconn->error, E_USER_ERROR);
	  }
	  $img_path="/usr/share/webapps/graffiti/images/".$this->targetName.".png";
	  $overlay_path="/usr/share/webapps/graffiti/overlays/".$this->targetName.".png";
	  $stmt->bind_param('sss', $this->targetName, $img_path, $overlay_path);
	  if (!$stmt->execute()) {
   		trigger_error("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
	  } else{
		//decode the image and overlay
		$image_bin = base64_decode($this->image);
		$overlay_bin = base64_decode($this->overlay);
		$imgfile = fopen($img_path, "w");
		$ovfile = fopen($overlay_path, "w");
		fwrite($imgfile, $image_bin);
		fwrite($ovfile, $overlay_bin);
		fclose($imgfile);
		fclose($ovfile);
	  }
          $stmt->close();
			
	}
	function execPostNewTarget(){

		$this->request = new HTTP_Request2();
		$this->request->setMethod( HTTP_Request2::METHOD_POST );
		$this->request->setBody( $this->jsonRequestObject );

		$this->request->setConfig(array(
		  'ssl_verify_peer'   => FALSE,
		  'ssl_verify_host'   => FALSE
		));

		$this->request->setURL( $this->url . $this->requestPath );

		// Define the Date and Authentication headers
		$this->setHeaders();


		try {

			$response = $this->request->send();

			
			$pos = strpos($response->getBody(), '{');
			$str = substr($response->getBody(), $pos);
			if($str == FALSE){
				echo json_encode(array('result_code'=>"Unknown error"));
			} else{
				echo $str;
			}
		} catch (HTTP_Request2_Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}


	}

	private function setHeaders(){
		$sb = 	new SignatureBuilder();
		$date = new DateTime("now", new DateTimeZone("GMT"));

		// Define the Date field using the proper GMT format
		$this->request->setHeader('Date', $date->format("D, d M Y H:i:s") . " GMT" );
		
		$this->request->setHeader("Content-Type", "application/json" );
		// Generate the Auth field value by concatenating the public server access key w/ the private query signature for this request
		$this->request->setHeader("Authorization" , "VWS " . $this->access_key . ":" . $sb->tmsSignature( $this->request , $this->secret_key ));

	}
	
}

?>
