#include "SearchActivity.hpp"



class SearchActivity_update_callback  : public QCAR::UpdateCallback
{
	virtual void QCAR_onUpdate(QCAR::State &state)
	{
		QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();

		QCAR::ImageTracker *image_tracker = static_cast<QCAR::ImageTracker*>(
                manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));

		QCAR::TargetFinder *finder = image_tracker->getTargetFinder();

		//check for results
		const int status = finder->updateSearchResults();

		if(status < 0){
			show_error_message(status, state.getFrame().getTimeStamp());
		} else if (status == QCAR::TargetFinder::UPDATE_RESULTS_AVAILABLE){
			//get searh results
			if (finder->getResultCount() > 0){
				const QCAR::TargetSearchResult* result = finder->getResult(0);
				//check suitability
				if (result->getTrackingRating() > 0){
					QCAR::Trackable* new_trackable = finder->enableTracking(*result);
					if(!new_trackable){
						return;
					}
					if(strcmp(result->getUniqueTargetId(), last_name)){
						current_overlay_is_stale = true;
						curr_state = REND_TARGET_FOUND_INIT;
						snprintf(last_metadata, MAX_SZ, "%s", result->getMetaData());
						create_overlay_texture(last_metadata);
					} else{
						curr_state = REND_NORMAL;
					}
					pthread_mutex_lock(&frameskip_mutex);
					frames_to_skip = 10;
					pthread_mutex_unlock(&frameskip_mutex);
					curr_tracking = false;

					//update the id and enter content mode in activity
					pthread_mutex_lock(&last_id_mutex);
					strcpy(last_name, result->getUniqueTargetId());
					pthread_mutex_unlock(&last_id_mutex);
					enter_content_mode();

				}
			}
		}
	}
};

SearchActivity_update_callback  update_callback;


JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setActivityPortraitMode(JNIEnv *, jobject, jboolean p)
{
	is_portrait = p;
}

JNIEXPORT int JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_initTracker(JNIEnv *, jobject)
{
	QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();
	QCAR::Tracker *tracker = manager.initTracker(QCAR::Tracker::IMAGE_TRACKER);

	if(!tracker){
		return 0;
	} else{
		return 1;
	}
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_deinitTracker(JNIEnv *, jobject)
{
	QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();
	manager.deinitTracker(QCAR::Tracker::IMAGE_TRACKER); //WHY THE FUCKING FUCK DOES THIS TAKE A FLAG? LET ALONE THE FLAG THAT INIT TOOK
}

JNIEXPORT int JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_initSearch(JNIEnv *, jobject)
{
	//get a tracker
	QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();
	QCAR::ImageTracker* image_tracker = static_cast<QCAR::ImageTracker*>(
            manager.getTracker(QCAR::Tracker::IMAGE_TRACKER)); //AGAIN WITH THE IMAGE TRACKER FLAG?! JESUS ASSMARMALADE CHRIST
	assert(image_tracker != NULL); //crash me hard, baby, if we didnt get a tracker

	//search start
	QCAR::TargetFinder *target_finder = image_tracker->getTargetFinder();
	if(target_finder){
		if(target_finder->startInit(kAccessKey, kSecretKey)){
			target_finder->waitUntilInitFinished();
		}
	} else{
		LOG("Image_tracker was null. bailing");
		return QCAR::TargetFinder::INIT_ERROR_SERVICE_NOT_AVAILABLE;
	}
	int result = target_finder->getInitState();
	if(result != QCAR::TargetFinder::INIT_SUCCESS){
		LOG("couldnt init finder. bailing");
		return result;
	}
	target_finder->setUIScanlineColor(0.2, 0.71, 0.90); //#HOLOYOLO
    target_finder->setUIPointColor(0.643, 0.776, 0.224); //android green
    return result;
} 

JNIEXPORT int JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_deinitSearch(JNIEnv *, jobject)
{
	QCAR::TrackerManager& tracker_manager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* image_tracker = static_cast<QCAR::ImageTracker*>(
            tracker_manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    if(!image_tracker){
    	return 0;
    }
    QCAR::TargetFinder* finder = image_tracker->getTargetFinder();
    finder->deinit();
    return 1;
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_overlayTextureIsCreated(JNIEnv *, jobject)
{
	curr_state = REND_TEXTURE_READY;
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_ui_SearchActivityRenderer_renderFrame(JNIEnv *, jobject)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	QCAR::State state = QCAR::Renderer::getInstance().begin();

	QCAR::Renderer::getInstance().drawVideoBackground();

	glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    if(current_overlay_is_stale){
    	if(curr_texture){
    		glDeleteTextures(1, &(curr_texture->mTextureID));
            delete curr_texture;
            curr_texture = NULL;
    	}
    	current_overlay_is_stale = false;
    }
    if(curr_state == REND_TEXTURE_READY){
    	generate_ogl_product_texture();
    }
    if(state.getNumTrackableResults() > 0){
    	curr_tracking = true;
    	pthread_mutex_lock(&frameskip_mutex);
        frames_to_skip = 0;
        pthread_mutex_unlock(&frameskip_mutex);

    	const QCAR::TrackableResult *result = state.getTrackableResult(0);
    	if(!result){
    		return;
    	}
    	view_mat = QCAR::Tool::convertPose2GLMatrix(result->getPose());

    	QCAR::ImageTargetResult *im_result = (QCAR::ImageTargetResult *)result;
    	target_sz = im_result->getTrackable().getSize();

    	render_augmentation(result);
    } else{

    	if(frames_to_skip > 0 && curr_state == REND_NORMAL){
    		pthread_mutex_lock(&frameskip_mutex);
    		frames_to_skip -= 1;
    		pthread_mutex_unlock(&frameskip_mutex);
    	}
    }

    QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker *tracker = static_cast<QCAR::ImageTracker*>(
            manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    QCAR::TargetFinder *finder = tracker->getTargetFinder();
    if(finder->isRequesting()){
    	set_ui_overlay_text("Searching");
    	show_ui_overlay();
    } else{
    	hide_ui_overlay();
    }
    glDisable(GL_DEPTH_TEST);

    QCAR::Renderer::getInstance().end();
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_initApplicationNative(
        JNIEnv* env, jobject obj, jint width, jint height)
{
	scr_width = width;
	scr_height = height;

	LOG("InitNative");

	jclass activity_class = env->GetObjectClass(obj);
	QCAR::registerCallback(&update_callback);

	show_error_ID = env->GetMethodID(activity_class, "showErrorMessage", "(I)V");
	create_overlay_texture_ID = env->GetMethodID(activity_class, "createOverlayTexture", "(Ljava/lang/String;)V"); //createOverlayTexture
    get_overlay_texture_ID = env->GetMethodID(activity_class, "getOverlayTexture", "()Lcom/polygonSoup/graffitiapp/utils/Texture;"); //getOverlayTexture
    show_status_ui_ID = env->GetMethodID(activity_class, "showToast", "()V"); //showToast
    hide_status_ui_ID = env->GetMethodID(activity_class, "hideToast", "()V"); //hideToast
    set_status_ui_text_ID = env->GetMethodID(activity_class, "setToastText", "(Ljava/lang/String;)V"); //setToastText
    enter_content_mode_ID = env->GetMethodID(activity_class,"enterContentMode", "()V");
    LOG("Registered handlers");
    activity = env->NewGlobalRef(obj);

    init_state_variables();
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_deinitApplicationNative(
        JNIEnv* env, jobject obj)
{
	if(activity){
		env->DeleteGlobalRef(activity);
		activity = NULL;
	}
	if(curr_texture){
		delete curr_texture;
		curr_texture = NULL;
	}
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_startCamera(JNIEnv *, jobject)
{
	LOG("StartCameraNative");
	if(!QCAR::CameraDevice::getInstance().init()){
		return;
	}
	LOG("Successfullystarted camera");

	configure_video_background();

	if (!QCAR::CameraDevice::getInstance().selectVideoMode(
                    QCAR::CameraDevice::MODE_DEFAULT)){
		return;
	}

    // Start the camera:
    if (!QCAR::CameraDevice::getInstance().start()){
    	return;
    }

    // Start the tracker:
    QCAR::TrackerManager& manager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* image_tracker = static_cast<QCAR::ImageTracker*>(
            manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    if(image_tracker){
    	image_tracker->start();
    }
    

    // Start cloud based recognition if we are in scanning mode:
    if(is_scanning){
        QCAR::TargetFinder *finder = image_tracker->getTargetFinder();
        if(finder){
        	is_started = finder->startRecognition();
        }
    }

}


JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_stopCamera(JNIEnv *,jobject)
{
	QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* image_tracker = static_cast<QCAR::ImageTracker*>(
            manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));

    if(image_tracker){
    	image_tracker->stop();
    }
    QCAR::TargetFinder *finder = image_tracker->getTargetFinder();
    if(finder){
    	is_started = !finder->stop();
    }

    finder->clearTrackables();

    QCAR::CameraDevice::getInstance().stop();
    QCAR::CameraDevice::getInstance().deinit();
	current_overlay_is_stale = true;

	init_state_variables();   
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setProjectionMatrix(JNIEnv *, jobject)
{
	const QCAR::CameraCalibration& calibration =
    QCAR::CameraDevice::getInstance().getCameraCalibration();
    proj_matrix = QCAR::Tool::getProjectionGL(calibration, 2.0f, 2500.0f);

    inv_proj_mat = SampleMath::Matrix44FInverse(proj_matrix);

}

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_isSearchStarted(JNIEnv*, jobject)
{
	return is_started ? JNI_TRUE : JNI_FALSE;
}


JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_activateFlash(JNIEnv*, jobject, jboolean flash)
{
	return QCAR::CameraDevice::getInstance().setFlashTorchMode((flash==JNI_TRUE)) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_autofocus(JNIEnv*, jobject)
{
	return QCAR::CameraDevice::getInstance().setFocusMode(QCAR::CameraDevice::FOCUS_MODE_TRIGGERAUTO) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setFocusMode(JNIEnv*, jobject, jint mode)
{
	int fmode;
	switch((int)mode){
		case 0:
			fmode = QCAR::CameraDevice::FOCUS_MODE_NORMAL;
			break;
		case 1:
			fmode = QCAR::CameraDevice::FOCUS_MODE_CONTINUOUSAUTO;
			break;
		case 2:
			fmode = QCAR::CameraDevice::FOCUS_MODE_INFINITY;
			break;
		case 3:
			fmode = QCAR::CameraDevice::FOCUS_MODE_MACRO;
			break;
		default:
			return JNI_FALSE;
	}
	return QCAR::CameraDevice::getInstance().setFocusMode(fmode) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_ui_SearchActivityRenderer_initRendering(
        JNIEnv* env, jobject obj)
 {
 	glClearColor(0.0f, 0.0f, 0.0f, QCAR::requiresAlpha() ? 0.0f : 1.0f);

 	shader_prgm_id = SampleUtils::createProgramFromBuffer(cubeMeshVertexShader,cubeFragmentShader);

 	vertex_handle = glGetAttribLocation(shader_prgm_id,"vertexPosition");
 	normal_handle = glGetAttribLocation(shader_prgm_id, "vertexNormal");
 	texture_coord_handle = glGetAttribLocation(shader_prgm_id, "vertexTexCoord");
    mvp_matrix_handle = glGetUniformLocation(shader_prgm_id,"modelViewProjectionMatrix");
 }

 JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_ui_SearchActivityRenderer_updateRendering(
        JNIEnv* env, jobject obj, jint width, jint height)
{
	scr_height = height;
	scr_width = width;
	configure_video_background();
}

JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM* vm, void* reserved)
{
	jvm = vm;
    return JNI_VERSION_1_4;
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_enterContentModeNative(JNIEnv*, jobject)
{
	QCAR::TrackerManager &manager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* image_tracker = static_cast<QCAR::ImageTracker*>(
            manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    if(image_tracker){
    	QCAR::TargetFinder *finder = image_tracker->getTargetFinder();
    	if(finder){
    		is_started = !finder->stop();
    		is_scanning = false;
    	}
    }
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_enterScanningModeNative(JNIEnv*, jobject)
{
	QCAR::TrackerManager& manager = QCAR::TrackerManager::getInstance();
    QCAR::ImageTracker* image_tracker = static_cast<QCAR::ImageTracker*>(
            manager.getTracker(QCAR::Tracker::IMAGE_TRACKER));
    if(image_tracker){
    	QCAR::TargetFinder* finder = image_tracker->getTargetFinder();
    	if(finder){
    		is_started = finder->startRecognition();
    		finder->clearTrackables();
    		is_scanning = true;
    		is_showing_2D_overlay = false;
    		curr_state = REND_NORMAL;
    	}
    }
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setDeviceDPIScaleFactor(JNIEnv*, jobject, jfloat dpiSIndicator)
{
	dpi_scale_indicator = dpiSIndicator;
	if(dpi_scale_indicator == 1.0f){
		scale_factor = 1.6f;
	} else if(dpi_scale_indicator == 1.5f){
		scale_factor = 1.3f;
	} else if(dpi_scale_indicator == 1.0f){
		scale_factor = 1.0f;
	}
}

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_cleanTargetTrackedID(JNIEnv*, jobject)
{
	pthread_mutex_lock(&last_id_mutex);
    last_name[0] = '\0';
    pthread_mutex_unlock(&last_id_mutex);
}

void show_error_message(int status_code, double frame_time)
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			if(frame_time > last_err_time + 2.5f){
				if(status_code != last_err_code){
					env->CallVoidMethod(activity, show_error_ID, status_code);
					last_err_time = frame_time;
					last_err_code = status_code;
				}
			}
		}
	}
}

void create_overlay_texture(const char *target_metadata)
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			env->CallVoidMethod(activity, create_overlay_texture_ID, env->NewStringUTF(target_metadata));
		}
	}
}

void generate_ogl_product_texture()
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			jobject texture = env->CallObjectMethod(activity, get_overlay_texture_ID);
			if(texture){
				curr_texture = Texture::create(env, texture);
			}
			glGenTextures(1, &(curr_texture->mTextureID));
        	glBindTexture(GL_TEXTURE_2D, curr_texture->mTextureID);
        	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024,
                1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

        	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, curr_texture->mWidth,
            	curr_texture->mHeight, GL_RGBA, GL_UNSIGNED_BYTE,
            	(GLvoid*) curr_texture->mData);

        	curr_state = REND_NORMAL;
        }
    }
}

void enter_content_mode()
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			env->CallVoidMethod(activity, enter_content_mode_ID);
		}
	}
}

void init_state_variables()
{
	curr_state = REND_NORMAL;

	curr_texture = NULL;

	pthread_mutex_lock(&last_id_mutex);
    last_name[0] = '\0';
    pthread_mutex_unlock(&last_id_mutex);

    is_scanning = true;
}

void render_augmentation(const QCAR::TrackableResult *trackable_result)
{
	QCAR::Matrix44F mvp;

	SampleUtils::scalePoseMatrix(430.f * scale_factor,
            430.f * scale_factor,
            1.0f,
            &view_mat.data[0]);
	SampleUtils::multiplyMatrix(&proj_matrix.data[0],
            &view_mat.data[0] ,
            &view_mat.data[0]);
	pose = trackable_result->getPose();
	glUseProgram(shader_prgm_id);
	if(curr_state == REND_NORMAL){
		glVertexAttribPointer(vertex_handle, 3, GL_FLOAT, GL_FALSE, 0,
                (const GLvoid*) &planeVertices[0]);
        glVertexAttribPointer(normal_handle, 3, GL_FLOAT, GL_FALSE, 0,
                (const GLvoid*) &planeNormals[0]);
        glVertexAttribPointer(texture_coord_handle, 2, GL_FLOAT, GL_FALSE, 0,
                (const GLvoid*) &planeTexcoords[0]);

        glEnableVertexAttribArray(vertex_handle);
        glEnableVertexAttribArray(normal_handle);
        glEnableVertexAttribArray(texture_coord_handle);

        // Enables Blending State
        glEnable(GL_BLEND);

        // Drawing Textured Plane
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, curr_texture->mTextureID);
        glUniformMatrix4fv(mvp_matrix_handle, 1, GL_FALSE,
                (GLfloat*)&view_mat.data[0] );
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT,
                (const GLvoid*) &planeIndices[0]);

        glDisableVertexAttribArray(vertex_handle);
        glDisableVertexAttribArray(normal_handle);
        glDisableVertexAttribArray(texture_coord_handle);

        // Disables Blending State - Its important to disable the blending state
        // after using it for preventing bugs with the Camera Video Background
        glDisable(GL_BLEND);
	}
}

void configure_video_background()
{
	QCAR::CameraDevice& camera = QCAR::CameraDevice::getInstance();
    QCAR::VideoMode video_mode = camera.getVideoMode(QCAR::CameraDevice::MODE_DEFAULT);

    QCAR::VideoBackgroundConfig config;
    config.mEnabled = true;
    config.mSynchronous = false;
    config.mPosition.data[0] = 0.0f;
    config.mPosition.data[1] = 0.0f;

    if(is_portrait){
    	config.mSize.data[0] = video_mode.mHeight * (scr_height / (float)video_mode.mWidth);
        config.mSize.data[1] = scr_height;

        if(config.mSize.data[0] < scr_width){
        	config.mSize.data[0] = scr_width;
            config.mSize.data[1] = scr_width * (video_mode.mWidth / (float)video_mode.mHeight);
        }
    } else{
    	config.mSize.data[0] = scr_width;
        config.mSize.data[1] = video_mode.mHeight * (scr_width / (float)video_mode.mWidth);

        if(config.mSize.data[1] < scr_height){
            config.mSize.data[0] = scr_height * (video_mode.mWidth / (float)video_mode.mHeight);
            config.mSize.data[1] = scr_height;
        }
    }
    QCAR::Renderer::getInstance().setVideoBackgroundConfig(config);
    LOG("configure_video_background");
}

void show_ui_overlay()
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			env->CallVoidMethod(activity, show_status_ui_ID);
		}
	}
}

void hide_ui_overlay()
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			env->CallVoidMethod(activity, hide_status_ui_ID);
		}
	}
}

void set_ui_overlay_text(const char *overlay_text)
{
	JNIEnv *env = NULL;
	if(jvm){
		if(jvm->GetEnv((void**)&env, JNI_VERSION_1_4) == JNI_OK){
			env->CallVoidMethod(activity, set_status_ui_text_ID, env->NewStringUTF(overlay_text));
		}
	}
}
#ifdef __cplusplus
}

#endif