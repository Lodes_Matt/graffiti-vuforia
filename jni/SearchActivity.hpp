#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <QCAR/QCAR.h>
#include <QCAR/UpdateCallback.h>
#include <QCAR/CameraDevice.h>
#include <QCAR/Renderer.h>
#include <QCAR/Area.h>
#include <QCAR/Rectangle.h>
#include <QCAR/VideoBackgroundConfig.h>
#include <QCAR/Trackable.h>
#include <QCAR/TrackableResult.h>
#include <QCAR/Tool.h>
#include <QCAR/Tracker.h>
#include <QCAR/TrackerManager.h>
#include <QCAR/ImageTracker.h>
#include <QCAR/ImageTargetResult.h>
#include <QCAR/CameraCalibration.h>
#include <QCAR/ImageTarget.h>
#include <QCAR/DataSet.h>
#include <QCAR/TargetFinder.h>
#include <QCAR/TargetSearchResult.h>
#include <QCAR/TrackableSource.h>
#include "SampleUtils.h"
#include "SampleMath.h"
#include "Texture.h"
#include "CubeShaders.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*
	openGL stuff for texture loading 
*/
static const float planeVertices[] =
{ -0.5, -0.5, 0.0, 0.5, -0.5, 0.0, 0.5, 0.5, 0.0, -0.5, 0.5, 0.0, };

static const float planeTexcoords[] =
{ 0.0, 0.0, 0.75f, 0.0, 0.75f, 0.75f, 0.0, 0.75f};

static const float planeNormals[] =
{ 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0 };

static const unsigned short planeIndices[] =
{ 0, 1, 2, 0, 2, 3 };


//matrices for displaying the ar content
QCAR::Matrix44F view_mat;
QCAR::Matrix44F inv_proj_mat;

/*
* native app part states
*/
static int REND_NORMAL = 0x0; //we have an active image and the overlay has been rendered on to it
static int REND_TARGET_LOST = 0xbaad; //target was lost, change back to scanning mode, render ui overlay
static int REND_TARGET_REFOUND = 0xf00d; //we found teh same target that we had. rerender
static int REND_TARGET_FOUND_INIT = 0xd00d; //initially found a target. lets grab some fucking textures and render that bitch
static int REND_TEXTURE_READY = 0xb00b; //we have our texture in java, ready to be generated in ogl
static int APP_SCANNING = 0xd00f; //were scanning. this is the initial state

static int curr_state = APP_SCANNING; //we start off scanning

float transition_duration = 0.5f;
bool is_showing_2D_overlay = false;


bool current_overlay_is_stale = false;


/*
 * pose and how big it is matrices/vectors
 */
QCAR::Matrix34F pose;
QCAR::Vec2F target_sz;


static const size_t MAX_SZ = 256;
char last_name[MAX_SZ]; //last image we found
char last_metadata[MAX_SZ];

pthread_mutex_t last_id_mutex;

Texture *curr_texture = NULL;

//found flag
bool curr_tracking = false;


//java methods
jmethodID create_overlay_texture_ID = 0;
jmethodID set_status_ui_text_ID = 0;
jmethodID show_error_ID = 0;
jmethodID hide_status_ui_ID = 0;
jmethodID show_status_ui_ID = 0;
jmethodID get_overlay_texture_ID = 0;
jmethodID enter_content_mode_ID = 0;

JavaVM *jvm = NULL;
jobject activity = NULL;


//drawing stuff
unsigned int shader_prgm_id = 0;
GLint vertex_handle = 0;
GLint normal_handle = 0;
GLint texture_coord_handle = 0;
GLint mvp_matrix_handle = 0;

unsigned int scr_height = 0;
unsigned int scr_width = 0;

bool is_portrait = false;

bool is_content = false;
bool is_scanning = false;

QCAR::Matrix44F proj_matrix;

static const char* kAccessKey = "338758f207a0419657a8defa0ab797a499f6859d";
static const char* kSecretKey = "e9e451d95b956a98b3e30da31e14cc298a2e819b";

bool is_started = false;

int frames_to_skip = 10;
pthread_mutex_t frameskip_mutex;

float scale_factor = 1;
float dpi_scale_indicator = 1;
double last_err_time = 0;
int last_err_code = 0;


//jni methods
JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setActivityPortraitMode(JNIEnv *, jobject, jboolean p);

JNIEXPORT int JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_initTracker(JNIEnv *, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_deinitTracker(JNIEnv *, jobject);

JNIEXPORT int JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_initSearch(JNIEnv *, jobject);

JNIEXPORT int JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_deinitSearch(JNIEnv *, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_overlayTextureIsCreated(JNIEnv *, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_ui_SearchActivityRenderer_renderFrame(JNIEnv *, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_initApplicationNative(
        JNIEnv* env, jobject obj, jint width, jint height);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_deinitApplicationNative(
        JNIEnv* env, jobject obj);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_startCamera(JNIEnv *, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_stopCamera(JNIEnv *,jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setProjectionMatrix(JNIEnv *, jobject);

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_isSearchStarted(JNIEnv*, jobject);

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_activateFlash(JNIEnv*, jobject, jboolean flash);

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_autofocus(JNIEnv*, jobject);

JNIEXPORT jboolean JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setFocusMode(JNIEnv*, jobject, jint mode);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_ui_SearchActivityRenderer_initRendering(
        JNIEnv* env, jobject obj);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_ui_SearchActivityRenderer_updateRendering(
        JNIEnv* env, jobject obj, jint width, jint height);

JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM* vm, void* reserved);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_enterContentModeNative(JNIEnv*, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_enterScanningModeNative(JNIEnv*, jobject);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_setDeviceDPIScaleFactor(JNIEnv*, jobject, jfloat dpiSIndicator);

JNIEXPORT void JNICALL
Java_com_polygonSoup_graffitiapp_vuforia_SearchActivity_cleanTargetTrackedID(JNIEnv*, jobject);


void show_error_message(int status_code, double frame_time);
void create_overlay_texture(const char *target_metadata);
void generate_ogl_product_texture();
void show_ui_overlay();
void hide_ui_overlay();
void set_ui_overlay_text(const char* overlay_text);
void configure_video_background();
void render_augmentation(const QCAR::TrackableResult* trackable_result);
void init_state_variables();
void enter_content_mode();

